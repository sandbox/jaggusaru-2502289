<?php

/**
 * @file
 * Contains education_detail.page.inc.
 *
 * Page callback for Education detail entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Education detail templates.
 *
 * Default template: education_detail.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_education_detail(array &$variables) {
  // Fetch EducationDetail Entity Object.
  $education_detail = $variables['elements']['#education_detail'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
