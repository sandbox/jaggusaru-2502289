<?php

/**
 * @file
 * Contains emergency_contact.page.inc.
 *
 * Page callback for Emergency contact entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Emergency contact templates.
 *
 * Default template: emergency_contact.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_emergency_contact(array &$variables) {
  // Fetch EmergencyContact Entity Object.
  $emergency_contact = $variables['elements']['#emergency_contact'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
