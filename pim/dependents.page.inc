<?php

/**
 * @file
 * Contains dependents.page.inc.
 *
 * Page callback for Dependents entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Dependents templates.
 *
 * Default template: dependents.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_dependents(array &$variables) {
  // Fetch Dependents Entity Object.
  $dependents = $variables['elements']['#dependents'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
