<?php

/**
 * @file
 * Contains skill.page.inc.
 *
 * Page callback for Skill entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Skill templates.
 *
 * Default template: skill.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_skill(array &$variables) {
  // Fetch Skill Entity Object.
  $skill = $variables['elements']['#skill'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
