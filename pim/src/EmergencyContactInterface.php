<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Emergency contact entities.
 *
 * @ingroup pim
 */
interface EmergencyContactInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Emergency contact name.
   *
   * @return string
   *   Name of the Emergency contact.
   */
  public function getName();

  /**
   * Sets the Emergency contact name.
   *
   * @param string $name
   *   The Emergency contact name.
   *
   * @return \Drupal\pim\EmergencyContactInterface
   *   The called Emergency contact entity.
   */
  public function setName($name);

  /**
   * Gets the Emergency contact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Emergency contact.
   */
  public function getCreatedTime();

  /**
   * Sets the Emergency contact creation timestamp.
   *
   * @param int $timestamp
   *   The Emergency contact creation timestamp.
   *
   * @return \Drupal\pim\EmergencyContactInterface
   *   The called Emergency contact entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Emergency contact published status indicator.
   *
   * Unpublished Emergency contact are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Emergency contact is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Emergency contact.
   *
   * @param bool $published
   *   TRUE to set this Emergency contact to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\EmergencyContactInterface
   *   The called Emergency contact entity.
   */
  public function setPublished($published);

}
