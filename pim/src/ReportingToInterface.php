<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Reporting to entities.
 *
 * @ingroup pim
 */
interface ReportingToInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Reporting to name.
   *
   * @return string
   *   Name of the Reporting to.
   */
  public function getName();

  /**
   * Sets the Reporting to name.
   *
   * @param string $name
   *   The Reporting to name.
   *
   * @return \Drupal\pim\ReportingToInterface
   *   The called Reporting to entity.
   */
  public function setName($name);

  /**
   * Gets the Reporting to creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reporting to.
   */
  public function getCreatedTime();

  /**
   * Sets the Reporting to creation timestamp.
   *
   * @param int $timestamp
   *   The Reporting to creation timestamp.
   *
   * @return \Drupal\pim\ReportingToInterface
   *   The called Reporting to entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Reporting to published status indicator.
   *
   * Unpublished Reporting to are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Reporting to is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Reporting to.
   *
   * @param bool $published
   *   TRUE to set this Reporting to to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\ReportingToInterface
   *   The called Reporting to entity.
   */
  public function setPublished($published);

}
