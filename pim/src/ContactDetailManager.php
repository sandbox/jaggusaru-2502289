<?php

namespace Drupal\pim;
use Drupal\pim\Entity\ContactDetail;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ContactDetailManager {

    public static function getContactByEmployeeId($employeeId) {
        $database = \Drupal::database();
        $contact = $database->select('contact_detail','cd')
                ->fields('cd')
                ->condition('employee_id', $employeeId)
                ->execute()
                ->fetch();
        return $contact;
    }

    /**
     * 
     * @param type $entity_type
     * @param type $data
     * @return type
     */
   public function insert($entity_type, $data)
   {
        $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew();
                    $status = $entity->save();
                    return $entity->employee_id->value;
   }
   /**
    * 
    * @param type $entity
    * @param type $data
    * @return type
    */
  public function update($entity_type, $data){
      $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew(false);
                    $status = $entity->save();
                    return $entity->employee_id->value;
  }
}
