<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Work experience entity.
 *
 * @see \Drupal\pim\Entity\WorkExperience.
 */
class WorkExperienceAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\WorkExperienceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished work experience entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published work experience entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit work experience entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete work experience entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add work experience entities');
  }

}
