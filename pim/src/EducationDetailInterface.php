<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Education detail entities.
 *
 * @ingroup pim
 */
interface EducationDetailInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Education detail name.
   *
   * @return string
   *   Name of the Education detail.
   */
  public function getName();

  /**
   * Sets the Education detail name.
   *
   * @param string $name
   *   The Education detail name.
   *
   * @return \Drupal\pim\EducationDetailInterface
   *   The called Education detail entity.
   */
  public function setName($name);

  /**
   * Gets the Education detail creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Education detail.
   */
  public function getCreatedTime();

  /**
   * Sets the Education detail creation timestamp.
   *
   * @param int $timestamp
   *   The Education detail creation timestamp.
   *
   * @return \Drupal\pim\EducationDetailInterface
   *   The called Education detail entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Education detail published status indicator.
   *
   * Unpublished Education detail are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Education detail is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Education detail.
   *
   * @param bool $published
   *   TRUE to set this Education detail to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\EducationDetailInterface
   *   The called Education detail entity.
   */
  public function setPublished($published);

}
