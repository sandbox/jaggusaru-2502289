<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Skill entities.
 *
 * @ingroup pim
 */
interface SkillInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Skill name.
   *
   * @return string
   *   Name of the Skill.
   */
  public function getName();

  /**
   * Sets the Skill name.
   *
   * @param string $name
   *   The Skill name.
   *
   * @return \Drupal\pim\SkillInterface
   *   The called Skill entity.
   */
  public function setName($name);

  /**
   * Gets the Skill creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Skill.
   */
  public function getCreatedTime();

  /**
   * Sets the Skill creation timestamp.
   *
   * @param int $timestamp
   *   The Skill creation timestamp.
   *
   * @return \Drupal\pim\SkillInterface
   *   The called Skill entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Skill published status indicator.
   *
   * Unpublished Skill are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Skill is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Skill.
   *
   * @param bool $published
   *   TRUE to set this Skill to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\SkillInterface
   *   The called Skill entity.
   */
  public function setPublished($published);

}
