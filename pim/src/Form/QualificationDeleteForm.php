<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Qualification entities.
 *
 * @ingroup pim
 */
class QualificationDeleteForm extends ContentEntityDeleteForm {

}
