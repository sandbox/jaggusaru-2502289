<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Emergency contact entities.
 *
 * @ingroup pim
 */
class EmergencyContactDeleteForm extends ContentEntityDeleteForm {

}
