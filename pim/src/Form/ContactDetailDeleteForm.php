<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Contact detail entities.
 *
 * @ingroup pim
 */
class ContactDetailDeleteForm extends ContentEntityDeleteForm {

}
