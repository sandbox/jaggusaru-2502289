<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Emergency contact edit forms.
 *
 * @ingroup pim
 */
class EmergencyContactForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pim\Entity\EmergencyContact */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Emergency contact.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Emergency contact.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.emergency_contact.canonical', ['emergency_contact' => $entity->id()]);
  }

}
