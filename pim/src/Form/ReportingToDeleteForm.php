<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Reporting to entities.
 *
 * @ingroup pim
 */
class ReportingToDeleteForm extends ContentEntityDeleteForm {

}
