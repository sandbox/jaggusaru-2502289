<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting License entities.
 *
 * @ingroup pim
 */
class LicenseDeleteForm extends ContentEntityDeleteForm {

}
