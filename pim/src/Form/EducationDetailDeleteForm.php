<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Education detail entities.
 *
 * @ingroup pim
 */
class EducationDetailDeleteForm extends ContentEntityDeleteForm {

}
