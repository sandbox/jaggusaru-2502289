<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Immigration entities.
 *
 * @ingroup pim
 */
class ImmigrationDeleteForm extends ContentEntityDeleteForm {

}
