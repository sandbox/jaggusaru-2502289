<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Employee entities.
 *
 * @ingroup pim
 */
class EmployeeDeleteForm extends ContentEntityDeleteForm {

}
