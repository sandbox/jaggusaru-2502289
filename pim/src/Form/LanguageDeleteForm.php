<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Language entities.
 *
 * @ingroup pim
 */
class LanguageDeleteForm extends ContentEntityDeleteForm {

}
