<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Salary entities.
 *
 * @ingroup pim
 */
class SalaryDeleteForm extends ContentEntityDeleteForm {

}
