<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job entities.
 *
 * @ingroup pim
 */
class JobDeleteForm extends ContentEntityDeleteForm {

}
