<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Skill entities.
 *
 * @ingroup pim
 */
class SkillDeleteForm extends ContentEntityDeleteForm {

}
