<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Dependents entities.
 *
 * @ingroup pim
 */
class DependentsDeleteForm extends ContentEntityDeleteForm {

}
