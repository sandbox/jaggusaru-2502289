<?php

namespace Drupal\pim\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Work experience entities.
 *
 * @ingroup pim
 */
class WorkExperienceDeleteForm extends ContentEntityDeleteForm {

}
