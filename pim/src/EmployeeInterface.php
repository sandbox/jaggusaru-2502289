<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Employee entities.
 *
 * @ingroup pim
 */
interface EmployeeInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

    // Add get/set methods for your configuration properties here.
    /**
     * Gets the Employee name.
     *
     * @return string
     *   Name of the Employee.
     */
    public function getName();

    /**
     * Sets the Employee name.
     *
     * @param string $name
     *   The Employee name.
     *
     * @return \Drupal\pim\EmployeeInterface
     *   The called Employee entity.
     */
    public function setName($name);

    /**
     * Gets the Employee creation timestamp.
     *
     * @return int
     *   Creation timestamp of the Employee.
     */
    public function getCreatedTime();

    /**
     * Sets the Employee creation timestamp.
     *
     * @param int $timestamp
     *   The Employee creation timestamp.
     *
     * @return \Drupal\pim\EmployeeInterface
     *   The called Employee entity.
     */
    public function setCreatedTime($timestamp);

    /**
     * Returns the Employee published status indicator.
     *
     * Unpublished Employee are only visible to restricted users.
     *
     * @return bool
     *   TRUE if the Employee is published.
     */
    public function isPublished();

    /**
     * Sets the published status of a Employee.
     *
     * @param bool $published
     *   TRUE to set this Employee to published, FALSE to set it to unpublished.
     *
     * @return \Drupal\pim\EmployeeInterface
     *   The called Employee entity.
     */
    public function setPublished($published);

    public function getFirstName();

    public function setFirstName($firstName);

    public function getMiddleName();

    public function setMiddleName($middleName);

    public function getLastName();

    public function setLastName($lastName);

    public function getDriverLicense();

    public function setDriverLicense($license);

    public function getLicenseExpiryDate();
    public function setLicenseExpiryDate($expiryDate);

    public function getGender();

    public function setGender($gender);

    public function getMaritalStatus();

    public function setMaritalStats($status);

    public function getNationality();

    public function setNationality($nationality);

    public function getBirthDate();

    public function setBirthDate($date);
}
