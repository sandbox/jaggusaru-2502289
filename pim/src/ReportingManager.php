<?php
namespace Drupal\pim;
use Drupal\pim\ReportingToInterface;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ReportingManager{
     /**
     * 
     * @param type $entity_type
     * @param type $data
     * @return type
     */
   public function insert($entity_type, $data)
   {
        $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew();
                    $status = $entity->save();
                    return $entity->employee_id->value;
   }
   /**
    * 
    * @param type $entity
    * @param type $data
    * @return type
    */
  public function update($entity_type, $data){
      $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew(false);
                    $status = $entity->save();
                    return $entity->employee_id->value;
  }
  public static function getAllManagersByEmployeeId($id) {
        $database = \Drupal::database();
        $managers = $database->select('reporting_to','i')
                ->fields('i')
                ->condition('employee_id', $id)
                ->condition('reporting_manager_type','manager')
                ->execute()
                ->fetchAll();
        return $managers;
    }
    public static function getAllSubordinatesByEmployeeId($id) {
        $database = \Drupal::database();
        $subordinates = $database->select('reporting_to','i')
                ->fields('i')
                ->condition('employee_id', $id)
                ->condition('reporting_manager_type','subordinate')
                ->execute()
                ->fetchAll();
        return $subordinates;
    }
    public function getEmployeeByJobtitile(){
         $database = \Drupal::database();
        $employee = $database->select('employee','i')
                ->fields('i')
               //condition('employee_id', $id)
                //->condition('role_id','manager')
                ->execute()
                ->fetchAll();
        return $employee;
    }
}