<?php
namespace Drupal\pim;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class PimManager{
 /**
     * Find the database id of a customer record. 
     * 
     * The customer data should include the following fields in order to 
     * get the unique id from the database: "email"
     * 
     * <strong>IMPORTANT!</strong> The record must already exists in the 
     * database, otherwise an exception is raised.
     * 
     * @param array $customer Array with the customer data. The 
     * keys of the array should have the same names as the db fields.
     * @return int Returns the id.
     */
    /*
    public function findRecordByEmail($data) {
        if (!isset($data['email'])) {
            throw new Exception('Customer\'s email was not provided : '
            . print_r($data, TRUE));
        }
        $database = \Drupal::database();
        $select = $database->select('provider', 'p');
        $select->fields('p', ['id']);
        $select->condition('email', mysql_real_escape_string($data['email']));
        //$database->condition('role_id', 'customer');
        return $select->execute()->fetchField();
    }
*/
    /**
     * Check if a particular customer record already exists.
     * 
     * This method checks wether the given customer already exists in 
     * the database. It doesn't search with the id, but with the following
     * fields: "email"
     * 
     * @param array $customer Associative array with the customer's 
     * data. Each key has the same name with the database fields.
     * @return bool Returns wether the record exists or not.
     */
    public function exists($user) {
        if (!isset($user['mail'])) {
            throw new Exception('Email is not provided.');
        }

        $conn = \Drupal::database();
        $result = $conn->query('select * from {user_field_Data} where mail=:email', [ ':email' => $user['mail']]);
        $user = $result->fetch();
        //echo count($customer);die;

        return (count($user) > 0) ? true : false;
    }
   
    
    public function validate_unique_email($email, $id) {
        $database = \Drupal::database();
        $select = $database->select('users_field_data', 'ufd');
        $select->fields('ufd');
        //$select->condition('uid', $id);
        $select->condition('mail', $email);
        $result = $select->execute()->fetch();
        return (count($result > 0)) ? FALSE : TRUE;
    }

    /**
     * Validate Records Username 
     * 
     * @param string $username The provider records username.
     * @param numeric $user_id The user record id.
     * @return bool Returns the validation result.
     */
    public function validate_username($username) {
        $connection = \Drupal::database();
        $select = $connection->select('users_field_data', 'ufd');
        $select->fields('ufd');
       // $select->condition('uid', $userId);
        $select->condition('name', $username);
        $result = $select->execute()->fetchAll();
        return (count($result) > 0) ? false : true;
        //echo count($result > 0) ? false : true;die;
        //return (count($result > 0)) ? FALSE : TRUE;
    }
}