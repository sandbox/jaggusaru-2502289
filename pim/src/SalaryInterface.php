<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Salary entities.
 *
 * @ingroup pim
 */
interface SalaryInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Salary name.
   *
   * @return string
   *   Name of the Salary.
   */
  public function getName();

  /**
   * Sets the Salary name.
   *
   * @param string $name
   *   The Salary name.
   *
   * @return \Drupal\pim\SalaryInterface
   *   The called Salary entity.
   */
  public function setName($name);

  /**
   * Gets the Salary creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Salary.
   */
  public function getCreatedTime();

  /**
   * Sets the Salary creation timestamp.
   *
   * @param int $timestamp
   *   The Salary creation timestamp.
   *
   * @return \Drupal\pim\SalaryInterface
   *   The called Salary entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Salary published status indicator.
   *
   * Unpublished Salary are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Salary is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Salary.
   *
   * @param bool $published
   *   TRUE to set this Salary to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\SalaryInterface
   *   The called Salary entity.
   */
  public function setPublished($published);

}
