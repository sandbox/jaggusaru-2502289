<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Contact detail entity.
 *
 * @see \Drupal\pim\Entity\ContactDetail.
 */
class ContactDetailAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\ContactDetailInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished contact detail entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published contact detail entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit contact detail entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete contact detail entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add contact detail entities');
  }

}
