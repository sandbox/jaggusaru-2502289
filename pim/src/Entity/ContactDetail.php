<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\ContactDetailInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Contact detail entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "contact_detail",
 *   label = @Translation("Contact detail"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\ContactDetailListBuilder",
 *     "views_data" = "Drupal\pim\Entity\ContactDetailViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\ContactDetailForm",
 *       "add" = "Drupal\pim\Form\ContactDetailForm",
 *       "edit" = "Drupal\pim\Form\ContactDetailForm",
 *       "delete" = "Drupal\pim\Form\ContactDetailDeleteForm",
 *     },
 *     "access" = "Drupal\pim\ContactDetailAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\ContactDetailHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "contact_detail",
 *   admin_permission = "administer contact detail entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/contact_detail/{contact_detail}",
 *     "add-form" = "/admin/structure/contact_detail/add",
 *     "edit-form" = "/admin/structure/contact_detail/{contact_detail}/edit",
 *     "delete-form" = "/admin/structure/contact_detail/{contact_detail}/delete",
 *     "collection" = "/admin/structure/contact_detail",
 *   },
 *   field_ui_base_route = "contact_detail.settings"
 * )
 */
class ContactDetail extends ContentEntityBase implements ContactDetailInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
     * {@inheritdoc}
     * @return type
     */
    public function getEmail() {
        return $this->get('email');
    }

    /**
     * {@inheritdoc}
     * @param type $email
     */
    public function setEmail($email) {

        $this->set('email', $email);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getLandlineNumber() {
        return $this->get('landline');
    }

    /**
     * {@inheritdoc}
     * @param type $landline
     */
    public function setLandlineNumber($landline) {
        $this->set('landline', $landline);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getMobileNumber() {
        return $this->get('mobile');
    }

    /**
     * {@inheritdoc}
     * @param type $number
     */
    public function setMobileNumber($number) {
        $this->set('mobile', $number);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getAddress() {
        return $this->get('address');
    }

    /**
     * {@inheritdoc}
     * @param type $address
     */
    public function setAddress($address) {
        $this->set('address', $address);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getCity() {
        return $this->get('city');
    }

    /**
     * {@inheritdoc}
     * @param type $city
     */
    public function setcity($city) {
        $this->set('city', $city);
    }

    /**
     * {@inheritdoc}
     * @param type $state
     */
    public function setState($state) {
        $this->set('state', $state);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getState() {
        return $this->get('state');
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getZipCode() {
        return $this->get('zipcode');
    }

    /**
     * {@inheritdoc}
     * @param type $zipCode
     */
    public function setZipCode($zipCode) {
        $this->set('zipcode', $zipcode);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getServiceId() {
        return $this->get('service_id');
    }

    /**
     * {@inheritdoc}
     * @param type $id
     */
    public function setServiceId($id) {
        $this->Set('service_id', $id);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getNotes() {
        return $this->get('notes');
    }

    /**
     * {@inheritdoc}
     * @param type $notes
     */
    public function setNotes($notes) {
        $this->set('notes', $notes);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getWorkingPlan() {
        return $this->get('working_plan');
    }

    /**
     * {@inheritdoc}
     * @param type $workingPlan
     */
    public function setWorkingPlan($workingPlan) {
        $this->set('working_plan', $workingPlan);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getAdvancedTimeOut() {
        return $this->get('advanced_book_timeout');
    }

    /**
     * {@inheritdoc}
     * @param type $timeOut
     */
    public function setAdvancedTimeOut($timeOut) {
        $this->set('advanced_book_timeout', $timeOut);
    }

    /**
     * {@inheritdoc}
     * @param type $notification
     */
    public function setNotifications($notification) {
        $this->set('notifications', $notification);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getNotifications() {
        return $this->get('notifications');
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getGoogleSync() {
        return $this->get('google_sync');
    }

    /**
     * {@inheritdoc}
     * @param type $sync
     */
    public function setGoogleSync($sync) {
        $this->set('google_sync', $sync);
    }

    /**
     * {@inheritdoc}
     */
    public function getGoogleTocken() {
        return $this->get('google_token');
    }

    /**
     * {@inheritdoc}
     * @param type $token
     */
    public function setGoogleTocken($token) {
        $this->set('google_token', $token);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getGoogleCalendar() {
        return $this->get('google_calendar');
    }

    /**
     * {@inheritdoc}
     * @param type $calendar
     */
    public function setGoogleCalendar($calendar) {
        $this->set('google_calendar', $calendar);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getSyncPastDays() {
        return $this->get('sync_past_days');
    }

    /**
     * {@inheritdoc}
     * @param type $days
     */
    public function setSyncPastDays($days) {
        $this->set('sync_past_days', $days);
    }

    /**
     * {@inheritdoc}
     * @return type
     */
    public function getSyncFutureDays() {
        return $this->get('sync_future_days');
    }

    /**
     * {@inheritdoc}
     * @param type $days
     */
    public function setSyncFutureDays($days) {
        $this->set('sync_future_days', $days);
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('ID'))
                ->setDescription(t('The ID of the services entity.'))
                ->setReadOnly(TRUE);
         $fields['employee_id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee ID'))
                ->setDescription(t('Reference ID.'));
                

        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the services entity.'))
                ->setReadOnly(TRUE);
        $fields['user_id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('User ID'))
                ->setDescription(t('The user ID of the service Provider.'));
        /*
          $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
          ->setLabel(t('Authored by'))
          ->setDescription(t('The user ID of the services entity author.'))
          ->setRevisionable(TRUE)
          ->setSetting('target_type', 'user')
          ->setSetting('handler', 'default')
          ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
          ->setTranslatable(TRUE)
          ->setDisplayOptions('view', array(
          'label' => 'hidden',
          'type' => 'author',
          'weight' => 0,
          ))
          ->setDisplayOptions('form', array(
          'type' => 'entity_reference_autocomplete',
          'weight' => 5,
          'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
          ),
          ))
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);
         */
         $fields['address_street'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Addres Street 1'))
                ->setDescription(t('Address Street 1.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
          $fields['address_street1'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Address Street 2'))
                ->setDescription(t('Address Street 2.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        
        
        $fields['city'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Last Name'))
                ->setDescription(t('The City of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['state'] = BaseFieldDefinition::create('string')
                ->setLabel(t('State'))
                ->setDescription(t('The State of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 2,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 2,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['zipcode'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Zipcode'))
                ->setDescription(t('The Zip code of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 3,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 3,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
       
        $fields['country'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Country'))
                ->setDescription(t('Country.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 15,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -2,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'options_select',
                    'weight' => -2,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['mobile'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Mobile Number'))
                ->setDescription(t('The Mobile Number of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['work_phone'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Work Telephone Number'))
                ->setDescription(t('Work Telephone Number.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 15,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -2,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -2,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
          $fields['home_phone'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Home Telephone Number'))
                ->setDescription(t('Home Telephone Number.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 15,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -2,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -2,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['work_email'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Work Email'))
                ->setDescription(t('Work Email.'));
        $fields['other_email'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Other Email'))
                ->setDescription(t('Other Email.'));


        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code of services entity.'));

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
