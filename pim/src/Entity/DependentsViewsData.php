<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Dependents entities.
 */
class DependentsViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['dependents']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Dependents'),
      'help' => $this->t('The Dependents ID.'),
    );

    return $data;
  }

}
