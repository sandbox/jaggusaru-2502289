<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Emergency contact entities.
 */
class EmergencyContactViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['emergency_contact']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Emergency contact'),
      'help' => $this->t('The Emergency contact ID.'),
    );

    return $data;
  }

}
