<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\EmployeeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Employee entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "employee",
 *   label = @Translation("Employee"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\EmployeeListBuilder",
 *     "views_data" = "Drupal\pim\Entity\EmployeeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\EmployeeForm",
 *       "add" = "Drupal\pim\Form\EmployeeForm",
 *       "edit" = "Drupal\pim\Form\EmployeeForm",
 *       "delete" = "Drupal\pim\Form\EmployeeDeleteForm",
 *     },
 *     "access" = "Drupal\pim\EmployeeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\EmployeeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "employee",
 *   admin_permission = "administer employee entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/employee/{employee}",
 *     "add-form" = "/admin/structure/employee/add",
 *     "edit-form" = "/admin/structure/employee/{employee}/edit",
 *     "delete-form" = "/admin/structure/employee/{employee}/delete",
 *     "collection" = "/admin/structure/employee",
 *   },
 *   field_ui_base_route = "employee.settings"
 * )
 */
class Employee extends ContentEntityBase implements EmployeeInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp) {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner() {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId() {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid) {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account) {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished() {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published) {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    public function getFirstName() {
        return $this->get('first_name');
    }

    public function setFirstName($firstName) {
        $this->set('first_name', $firstName);
    }

    public function getMiddleName() {
        return $this->get('middle_name');
    }

    public function setMiddleName($middleName) {
        $this->set('middle_name', $middleName);
    }

    public function getLastName() {
        return $this->get('last_name');
    }

    public function setLastName($lastName) {
        $this->set('last_name', $lastName);
    }

    public function getDriverLicense() {
        return $this->get('driver_license');
    }

    public function setDriverLicense($license) {
        $this->set('driver_license', $license);
    }

    public function getLicenseExpiryDate() {
        return $this->get('license_expiry_date');
    }

    public function setLicenseExpiryDate($expiryDate) {
        return $this->set('license_expiry_date', $expiryDate);
    }

    public function getGender() {
        return $this->get('gender');
    }

    public function setGender($gender) {
        $this->set('gender', $gender);
    }

    public function getMaritalStatus() {
        $this->get('marital_status');
    }

    public function setMaritalStats($status) {
        $this->set('marital_status', $status);
    }

    public function getNationality() {
        return $this->get('nationality');
    }

    public function setNationality($nationality) {
        $this->set('nationality', $nationality);
    }

    public function getBirthDate() {
        return $this->get('birth_date');
    }

    public function setBirthDate($date) {
        $this->set('birth_date', $date);
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee ID'))
                ->setDescription(t('The ID of the Employee entity.'))
                ->setReadOnly(TRUE);
        $fields['employee_status'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee Status'))
                ->setDescription(t('Employee Status.'));
               
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the Employee entity.'))
                ->setReadOnly(TRUE);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
                ->setLabel(t('Authored by'))
                ->setDescription(t('The user ID of author of the Employee entity.'))
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'user')
                ->setSetting('handler', 'default')
                ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
                ->setTranslatable(TRUE)
                ->setDisplayOptions('view', array(
                    'label' => 'hidden',
                    'type' => 'author',
                    'weight' => 0,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'entity_reference_autocomplete',
                    'weight' => 5,
                    'settings' => array(
                        'match_operator' => 'CONTAINS',
                        'size' => '60',
                        'autocomplete_type' => 'tags',
                        'placeholder' => '',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Name'))
                ->setDescription(t('The name of the Employee entity.'))
                ->setSettings(array(
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['first_name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('First Name'))
                ->setDescription(t('The First name of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                    'settings' => array(
                        'prefix' => '<div>',
                        'suffix' => '</div>',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['middle_name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Middle Name'))
                ->setDescription(t('The First name of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                    'settings' => array(
                        'prefix' => '<div>',
                        'suffix' => '</div>',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['last_name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Last Name'))
                ->setDescription(t('The Last name of Service Provider.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -3,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -3,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['driver_license'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Driver\'s License Number'))
                ->setDescription(t('Driver\'s License Number.'))
                ->setSettings(array(
                    'default_value' => '',
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -2,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string',
                    'weight' => -2,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['license_expiry_date'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Driver\'s License Expiry Date'))
                ->setDescription(t('Driver\'s License Expiry Date.'));
        $fields['gender'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Gender'))
                ->setDescription(t('Gender.'));
        $fields['marital_status'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Marital Status'))
                ->setDescription(t('Marital Status.'));
        $fields['nationality'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Nationality'))
                ->setDescription(t('Nationality.'));
        $fields['birth_date'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Birthday'))
                ->setDescription(t('Birthday.'));
        $fields['status'] = BaseFieldDefinition::create('boolean')
                ->setLabel(t('Publishing status'))
                ->setDescription(t('A boolean indicating whether the Employee is published.'))
                ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code for the Employee entity.'))
                ->setDisplayOptions('form', array(
                    'type' => 'language_select',
                    'weight' => 10,
                ))
                ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
