<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\ImmigrationInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Immigration entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "immigration",
 *   label = @Translation("Immigration"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\ImmigrationListBuilder",
 *     "views_data" = "Drupal\pim\Entity\ImmigrationViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\ImmigrationForm",
 *       "add" = "Drupal\pim\Form\ImmigrationForm",
 *       "edit" = "Drupal\pim\Form\ImmigrationForm",
 *       "delete" = "Drupal\pim\Form\ImmigrationDeleteForm",
 *     },
 *     "access" = "Drupal\pim\ImmigrationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\ImmigrationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "immigration",
 *   admin_permission = "administer immigration entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/immigration/{immigration}",
 *     "add-form" = "/admin/structure/immigration/add",
 *     "edit-form" = "/admin/structure/immigration/{immigration}/edit",
 *     "delete-form" = "/admin/structure/immigration/{immigration}/delete",
 *     "collection" = "/admin/structure/immigration",
 *   },
 *   field_ui_base_route = "immigration.settings"
 * )
 */
class Immigration extends ContentEntityBase implements ImmigrationInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp) {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner() {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId() {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid) {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account) {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished() {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published) {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    public function getDocument() {
        return $this->get('document');
    }

    public function setDocument($docuement) {
        $this->set('document', $document);
    }

    public function getDocumentNumber() {
        return $this->get('number');
    }

    public function setDocumentNumber($number) {
        $this->set('number', $number);
    }

    public function getDocumentIssuer() {
        return $this->get('issued_by');
    }

    public function setDocumentIssuer($issyedBy) {
        $this->set('issued_by', $issuedBy);
    }

    public function getExpiryDate() {
        return $this->get('expiry_date');
    }

    public function setExpiryDate($expiryDate) {
        $this->set('expiry_date', $expiryDate);
    }

    public function getEligibleReviewDate() {
        return $this->get('eligible_review_date');
    }

    public function setEligibleReviewDate($reviewDate) {
        $this->set('eligible_review_date', $reviewDate);
    }

    public function getComment() {
        return $this->get('comment');
    }

    public function setComment($comment) {
        $this->set('comment', $comment);
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('ID'))
                ->setDescription(t('The ID of the Immigration entity.'))
                ->setReadOnly(TRUE);
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the Immigration entity.'))
                ->setReadOnly(TRUE);
        $fields['employee_id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee ID'))
                ->setDescription(t('Reference ID.'));
                

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
                ->setLabel(t('Authored by'))
                ->setDescription(t('The user ID of author of the Immigration entity.'))
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'user')
                ->setSetting('handler', 'default')
                ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
                ->setTranslatable(TRUE)
                ->setDisplayOptions('view', array(
                    'label' => 'hidden',
                    'type' => 'author',
                    'weight' => 0,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'entity_reference_autocomplete',
                    'weight' => 5,
                    'settings' => array(
                        'match_operator' => 'CONTAINS',
                        'size' => '60',
                        'autocomplete_type' => 'tags',
                        'placeholder' => '',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Name'))
                ->setDescription(t('The name of the Immigration entity.'))
                ->setSettings(array(
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['document'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Document'))
                ->setDescription(t('Document.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['number'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Document Number'))
                ->setDescription(t('Document Number.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['issued_by'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Issued By'))
                ->setDescription(t('Document Issued By.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['issued_date'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Issued Date'))
                ->setDescription(t('Document Issued Date.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['expiry_date'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Expiry Date'))
                ->setDescription(t('Document Expiry Date.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['eligible_review_date'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Eligible Review Date'))
                ->setDescription(t('Eligible Review Date.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['comment'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Issued Date'))
                ->setDescription(t('Document Issued Date.'))
                ->setSettings(array(
                    'default_value' => '',
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => 1,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => 1,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['status'] = BaseFieldDefinition::create('boolean')
                ->setLabel(t('Publishing status'))
                ->setDescription(t('A boolean indicating whether the Immigration is published.'))
                ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code for the Immigration entity.'))
                ->setDisplayOptions('form', array(
                    'type' => 'language_select',
                    'weight' => 10,
                ))
                ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
