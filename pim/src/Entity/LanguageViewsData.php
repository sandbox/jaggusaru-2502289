<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Language entities.
 */
class LanguageViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['language']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Language'),
      'help' => $this->t('The Language ID.'),
    );

    return $data;
  }

}
