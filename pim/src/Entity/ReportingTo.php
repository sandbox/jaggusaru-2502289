<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\ReportingToInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Reporting to entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "reporting_to",
 *   label = @Translation("Reporting to"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\ReportingToListBuilder",
 *     "views_data" = "Drupal\pim\Entity\ReportingToViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\ReportingToForm",
 *       "add" = "Drupal\pim\Form\ReportingToForm",
 *       "edit" = "Drupal\pim\Form\ReportingToForm",
 *       "delete" = "Drupal\pim\Form\ReportingToDeleteForm",
 *     },
 *     "access" = "Drupal\pim\ReportingToAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\ReportingToHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "reporting_to",
 *   admin_permission = "administer reporting to entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/reporting_to/{reporting_to}",
 *     "add-form" = "/admin/structure/reporting_to/add",
 *     "edit-form" = "/admin/structure/reporting_to/{reporting_to}/edit",
 *     "delete-form" = "/admin/structure/reporting_to/{reporting_to}/delete",
 *     "collection" = "/admin/structure/reporting_to",
 *   },
 *   field_ui_base_route = "reporting_to.settings"
 * )
 */
class ReportingTo extends ContentEntityBase implements ReportingToInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }
  public function setEmployeeId($id){
      $this->set('employee_id', $id);
      
  }
  public function getEmployeeId($id){
      return $this->get('employee_id');
  }
  public function setReportingManager($manager){
      $this->set('reporting_manager_id', $manager);
  }
  public function getReportingManager(){
      return $this->get('reporting_manager_id');
  }
  public function setReportingMethod($method){
      $this->set('reporting_method', $method);
  }
  public function getReportingMethod(){
      return $this->get('reporting_method');
  }
  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Reporting to entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Reporting to entity.'))
      ->setReadOnly(TRUE);
    $fields['employee_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Employee ID'))
      ->setDescription(t('Employee ID'));
    $fields['reporting_manager_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Reporting Manager ID'))
      ->setDescription(t('Reporting Manager ID'));
    $fields['reporting_method'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Reporting Method'))
      ->setDescription(t('Reporting Method.'))
      ->setSettings(array(
        'max_length' => 250,
        'text_processing' => 0,
      ));
        $fields['reporting_manager_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Supervisor or Subordinate'))
      ->setDescription(t('Supervisor or Subordinate.'))
      ->setSettings(array(
        'max_length' => 250,
        'text_processing' => 0,
      ));
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Reporting to entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Reporting to entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Reporting to is published.'))
      ->setDefaultValue(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Reporting to entity.'))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
