<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Immigration entities.
 */
class ImmigrationViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['immigration']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Immigration'),
      'help' => $this->t('The Immigration ID.'),
    );

    return $data;
  }

}
