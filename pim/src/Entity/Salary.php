<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\SalaryInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Salary entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "salary",
 *   label = @Translation("Salary"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\SalaryListBuilder",
 *     "views_data" = "Drupal\pim\Entity\SalaryViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\SalaryForm",
 *       "add" = "Drupal\pim\Form\SalaryForm",
 *       "edit" = "Drupal\pim\Form\SalaryForm",
 *       "delete" = "Drupal\pim\Form\SalaryDeleteForm",
 *     },
 *     "access" = "Drupal\pim\SalaryAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\SalaryHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "salary",
 *   admin_permission = "administer salary entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/salary/{salary}",
 *     "add-form" = "/admin/structure/salary/add",
 *     "edit-form" = "/admin/structure/salary/{salary}/edit",
 *     "delete-form" = "/admin/structure/salary/{salary}/delete",
 *     "collection" = "/admin/structure/salary",
 *   },
 *   field_ui_base_route = "salary.settings"
 * )
 */
class Salary extends ContentEntityBase implements SalaryInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp) {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner() {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId() {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid) {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account) {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished() {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published) {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    public function setEmployeeId($id) {
        $this->set('employee_id', $id);
    }

    public function getEmployeeId() {
        return $this->get('employee_id');
    }

    public function setPayGrade($payGarade) {
        $this->set('pay_grade', $payload);
    }

    public function getPayGrade() {
        return $this->get('pay_grade');
    }

    public function setSalaryComponent($component) {
        $this->set('salary_component', $compnenet);
    }

    public function getSalaryComponent() {
        return $this->get('salary_component');
    }

    public function setPayFrequency($frequency) {
        $this->set('pay_frequency', $frequency);
    }

    public function getPayFrequency() {
        return $this->get('pay_frequency');
    }

    public function setAmount($amount) {
        $this->set('amount', $amount);
    }

    public function getAmount() {
        return $this->get('amount');
    }

    public function setComment($comment) {
        $this->set('comment', $comment);
    }

    public function getComment() {
        return $this->get('comment');
    }

    public function setAccountNumber($acNumber) {
        $this->set('account_number', $acNumber);
    }

    public function getAccountNumber() {
        return $this->get('account_number');
    }

    public function setDirectoDepositAmount($amt) {
        $this->set('direct_deposit_amount', $amt);
    }

    public function getDirectDepositamout() {
        return $this->get('direct_deposit_amount');
    }

    public function setAccountType($type) {
        $this->set('account_type', $type);
    }

    public function getAccountType() {
        return $this->get('account_type');
    }

    public function setRoutingNumber($number) {
        $this->set('routing_number', $number);
    }

    public function getRoutingNumber() {
        return $this->get('routing_number');
    }

    /*
      pay_grade
      salary_component
      pay_frequency
      currency
      amount
      comment
      account_number
      direct_deposit_amount
      account_type
      routing_number
     */

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('ID'))
                ->setDescription(t('The ID of the Salary entity.'))
                ->setReadOnly(TRUE);
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the Salary entity.'))
                ->setReadOnly(TRUE);


        $fields['pay_grade'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Pay Grade'))
                ->setDescription(t('Pay Grade.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['salary_component'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Salary Component'))
                ->setDescription(t('Salary Component.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['pay_frequency'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Pay Frequency'))
                ->setDescription(t('Pay Frequency.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['currency'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Currency'))
                ->setDescription(t('Currency.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['amount'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Amount'))
                ->setDescription(t('Amount.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['comment'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Comment'))
                ->setDescription(t('Comment.'))
                ->setSettings(array(
                    'max_length' => 512,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['account_number'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Account Number'))
                ->setDescription(t('Account Number.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['account_type'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Account Type'))
                ->setDescription(t('Account type.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        $fields['routing_number'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Routing Number'))
                ->setDescription(t('Routing Number.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['direct_deposit_amount'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Direct Deposit Amount'))
                ->setDescription(t('Direct Deposit Amount.'))
                ->setSettings(array(
                    'max_length' => 256,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
                ->setLabel(t('Authored by'))
                ->setDescription(t('The user ID of author of the Salary entity.'))
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'user')
                ->setSetting('handler', 'default')
                ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
                ->setTranslatable(TRUE)
                ->setDisplayOptions('view', array(
                    'label' => 'hidden',
                    'type' => 'author',
                    'weight' => 0,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'entity_reference_autocomplete',
                    'weight' => 5,
                    'settings' => array(
                        'match_operator' => 'CONTAINS',
                        'size' => '60',
                        'autocomplete_type' => 'tags',
                        'placeholder' => '',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);



        $fields['status'] = BaseFieldDefinition::create('boolean')
                ->setLabel(t('Publishing status'))
                ->setDescription(t('A boolean indicating whether the Salary is published.'))
                ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code for the Salary entity.'))
                ->setDisplayOptions('form', array(
                    'type' => 'language_select',
                    'weight' => 10,
                ))
                ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));
        $fields['employee_id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee ID'))
                ->setDescription(t('The Employee ID.'));
        return $fields;
    }

}
