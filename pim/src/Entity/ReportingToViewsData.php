<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Reporting to entities.
 */
class ReportingToViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['reporting_to']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Reporting to'),
      'help' => $this->t('The Reporting to ID.'),
    );

    return $data;
  }

}
