<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Education detail entities.
 */
class EducationDetailViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['education_detail']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Education detail'),
      'help' => $this->t('The Education detail ID.'),
    );

    return $data;
  }

}
