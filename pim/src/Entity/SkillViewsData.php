<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Skill entities.
 */
class SkillViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['skill']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Skill'),
      'help' => $this->t('The Skill ID.'),
    );

    return $data;
  }

}
