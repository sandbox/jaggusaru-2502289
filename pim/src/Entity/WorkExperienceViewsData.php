<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Work experience entities.
 */
class WorkExperienceViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['work_experience']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Work experience'),
      'help' => $this->t('The Work experience ID.'),
    );

    return $data;
  }

}
