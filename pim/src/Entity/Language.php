<?php

namespace Drupal\pim\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\pim\LanguageInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Language entity.
 *
 * @ingroup pim
 *
 * @ContentEntityType(
 *   id = "language",
 *   label = @Translation("Language"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pim\LanguageListBuilder",
 *     "views_data" = "Drupal\pim\Entity\LanguageViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\pim\Form\LanguageForm",
 *       "add" = "Drupal\pim\Form\LanguageForm",
 *       "edit" = "Drupal\pim\Form\LanguageForm",
 *       "delete" = "Drupal\pim\Form\LanguageDeleteForm",
 *     },
 *     "access" = "Drupal\pim\LanguageAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\pim\LanguageHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "language",
 *   admin_permission = "administer language entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/language/{language}",
 *     "add-form" = "/admin/structure/language/add",
 *     "edit-form" = "/admin/structure/language/{language}/edit",
 *     "delete-form" = "/admin/structure/language/{language}/delete",
 *     "collection" = "/admin/structure/language",
 *   },
 *   field_ui_base_route = "language.settings"
 * )
 */
class Language extends ContentEntityBase implements LanguageInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp) {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner() {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId() {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid) {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account) {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished() {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published) {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('ID'))
                ->setDescription(t('The ID of the Language entity.'))
                ->setReadOnly(TRUE);
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the Language entity.'))
                ->setReadOnly(TRUE);
        $fields['employee_id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('Employee ID'))
                ->setDescription(t('The Employee ID.'));
        $fields['language'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Language'))
                ->setDescription(t('Language'))
                ->setSettings(array(
            'max_length' => 256,
            'text_processing' => 0,
        ));
        $fields['fluency'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Fluency'))
                ->setDescription(t('Fluency'))
                ->setSettings(array(
            'max_length' => 256,
            'text_processing' => 0,
        ));

        $fields['competency'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Competency'))
                ->setDescription(t('Competency.'))
                ->setSettings(array(
            'max_length' => 256,
            'text_processing' => 0,
        ));
        $fields['comment'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Comment'))
                ->setDescription(t('Comment'))
                ->setSettings(array(
            'max_length' => 256,
            'text_processing' => 0,
        ));
        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
                ->setLabel(t('Authored by'))
                ->setDescription(t('The user ID of author of the Language entity.'))
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'user')
                ->setSetting('handler', 'default')
                ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
                ->setTranslatable(TRUE)
                ->setDisplayOptions('view', array(
                    'label' => 'hidden',
                    'type' => 'author',
                    'weight' => 0,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'entity_reference_autocomplete',
                    'weight' => 5,
                    'settings' => array(
                        'match_operator' => 'CONTAINS',
                        'size' => '60',
                        'autocomplete_type' => 'tags',
                        'placeholder' => '',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Name'))
                ->setDescription(t('The name of the Language entity.'))
                ->setSettings(array(
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['status'] = BaseFieldDefinition::create('boolean')
                ->setLabel(t('Publishing status'))
                ->setDescription(t('A boolean indicating whether the Language is published.'))
                ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code for the Language entity.'))
                ->setDisplayOptions('form', array(
                    'type' => 'language_select',
                    'weight' => 10,
                ))
                ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
