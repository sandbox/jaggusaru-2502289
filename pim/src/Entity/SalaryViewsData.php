<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Salary entities.
 */
class SalaryViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['salary']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Salary'),
      'help' => $this->t('The Salary ID.'),
    );

    return $data;
  }

}
