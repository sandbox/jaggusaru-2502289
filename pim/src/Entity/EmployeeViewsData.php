<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Employee entities.
 */
class EmployeeViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['employee']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Employee'),
      'help' => $this->t('The Employee ID.'),
    );

    return $data;
  }

}
