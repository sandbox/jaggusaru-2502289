<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Contact detail entities.
 */
class ContactDetailViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['contact_detail']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Contact detail'),
      'help' => $this->t('The Contact detail ID.'),
    );

    return $data;
  }

}
