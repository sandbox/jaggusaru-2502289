<?php

namespace Drupal\pim\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Qualification entities.
 */
class QualificationViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['qualification']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Qualification'),
      'help' => $this->t('The Qualification ID.'),
    );

    return $data;
  }

}
