<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Qualification entities.
 *
 * @ingroup pim
 */
interface QualificationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Qualification name.
   *
   * @return string
   *   Name of the Qualification.
   */
  public function getName();

  /**
   * Sets the Qualification name.
   *
   * @param string $name
   *   The Qualification name.
   *
   * @return \Drupal\pim\QualificationInterface
   *   The called Qualification entity.
   */
  public function setName($name);

  /**
   * Gets the Qualification creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Qualification.
   */
  public function getCreatedTime();

  /**
   * Sets the Qualification creation timestamp.
   *
   * @param int $timestamp
   *   The Qualification creation timestamp.
   *
   * @return \Drupal\pim\QualificationInterface
   *   The called Qualification entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Qualification published status indicator.
   *
   * Unpublished Qualification are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Qualification is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Qualification.
   *
   * @param bool $published
   *   TRUE to set this Qualification to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\QualificationInterface
   *   The called Qualification entity.
   */
  public function setPublished($published);

}
