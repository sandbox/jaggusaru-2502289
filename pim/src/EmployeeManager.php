<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\pim;

use Drupal\pim\entity\Employee;

class EmployeeManager {

    const TERMINATED = 1;

    /**
     * 
     * @param type $entity_type
     * @param type $data
     * @return type
     */
    public function insert($entity_type, $data) {
        $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
        $entity->enforceIsNew();
        $status = $entity->save();
        return $entity->id->value;
    }

    /**
     * 
     * @param type $entity
     * @param type $data
     * @return type
     */
    public function update($entity_type, $data) {
        $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
        $entity->enforceIsNew(false);
        $status = $entity->save();
        return $entity->id->value;
    }

    public function getAllEmployees() {
        $database = \Drupal::database();
       // $q = $database->query('select e.id, e.first_name, e.last_name, j.job_title, rt.reporting_manager_id from (employee e inner join job j on e.id = j.employee_id) left inner join reporting_to rt on j.employee_id = rt.employee_id');
       // select * from (bdg left join res on bdg.bid = res.bid) left join dom on res.rid = dom.rid;
      //  select e.id, e.first_name, e.last_name, j.job_titile, rt.reporting_manager_id from (employee e left join job j on e.id = j.employee_id) left join reporting_to rt on j.employee_id = rt.employee_id;
      $select = $database->select('employee', 'e');
      
        $select->Join('job', 'j', 'e.id = j.employee_id');
       
       $select->Join('reporting_to', 'rt', 'j.employee_id = rt.employee_id');
        $select->fields('e', ['id', 'first_name', 'last_name']);
        // $select->groupBy('id');
        $select->fields('j', ['job_title', 'employment_status', 'sub_unit']);
       $select->fields('rt', ['reporting_manager_id']);
      

        $employees = $select->execute()->fetchAll();
       
       // $employee = $q->fetchAll();
        return $employees;
    }
    public function getAllSuperVisors(){
       
         $database = \Drupal::database();
        $employee = $database->select('employee','i')
                ->fields('i')
               //condition('employee_id', $id)
                //->condition('role_id','manager')
                ->execute()
                ->fetchAll();
        return $employee;
    }
    public function getEmployeeBySearchCriteria($id ){
        
         $database = \Drupal::database();
        $employee = $database->select('employee','i')
                ->fields('i')
               //condition('employee_id', $id)
               ->condition('id',$id)
                ->execute()
                ->fetch();
        return $employee;
    }
    public function getUserById($id){
         $query = \Drupal::entityQuery('user')
              ->condition('uid', $id);
       $nid = $query->execute();
        return File::load($nid);
         
   
    }
}
