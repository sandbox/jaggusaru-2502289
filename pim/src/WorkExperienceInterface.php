<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Work experience entities.
 *
 * @ingroup pim
 */
interface WorkExperienceInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Work experience name.
   *
   * @return string
   *   Name of the Work experience.
   */
  public function getName();

  /**
   * Sets the Work experience name.
   *
   * @param string $name
   *   The Work experience name.
   *
   * @return \Drupal\pim\WorkExperienceInterface
   *   The called Work experience entity.
   */
  public function setName($name);

  /**
   * Gets the Work experience creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Work experience.
   */
  public function getCreatedTime();

  /**
   * Sets the Work experience creation timestamp.
   *
   * @param int $timestamp
   *   The Work experience creation timestamp.
   *
   * @return \Drupal\pim\WorkExperienceInterface
   *   The called Work experience entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Work experience published status indicator.
   *
   * Unpublished Work experience are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Work experience is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Work experience.
   *
   * @param bool $published
   *   TRUE to set this Work experience to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\WorkExperienceInterface
   *   The called Work experience entity.
   */
  public function setPublished($published);

}
