<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Immigration entities.
 *
 * @ingroup pim
 */
interface ImmigrationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Immigration name.
   *
   * @return string
   *   Name of the Immigration.
   */
  public function getName();

  /**
   * Sets the Immigration name.
   *
   * @param string $name
   *   The Immigration name.
   *
   * @return \Drupal\pim\ImmigrationInterface
   *   The called Immigration entity.
   */
  public function setName($name);

  /**
   * Gets the Immigration creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Immigration.
   */
  public function getCreatedTime();

  /**
   * Sets the Immigration creation timestamp.
   *
   * @param int $timestamp
   *   The Immigration creation timestamp.
   *
   * @return \Drupal\pim\ImmigrationInterface
   *   The called Immigration entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Immigration published status indicator.
   *
   * Unpublished Immigration are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Immigration is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Immigration.
   *
   * @param bool $published
   *   TRUE to set this Immigration to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\ImmigrationInterface
   *   The called Immigration entity.
   */
  public function setPublished($published);

}
