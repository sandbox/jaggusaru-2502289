<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Qualification entity.
 *
 * @see \Drupal\pim\Entity\Qualification.
 */
class QualificationAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\QualificationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished qualification entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published qualification entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit qualification entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete qualification entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add qualification entities');
  }

}
