<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Education detail entity.
 *
 * @see \Drupal\pim\Entity\EducationDetail.
 */
class EducationDetailAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\EducationDetailInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished education detail entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published education detail entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit education detail entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete education detail entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add education detail entities');
  }

}
