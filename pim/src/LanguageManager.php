<?php
namespace Drupal\pim;
use Drupal\pim\LanguageInterface;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LanguageManager{
     /**
     * 
     * @param type $entity_type
     * @param type $data
     * @return type
     */
   public function insert($entity_type, $data)
   {
        $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew();
                    $status = $entity->save();
                    return $entity->employee_id->value;
   }
   /**
    * 
    * @param type $entity
    * @param type $data
    * @return type
    */
  public function update($entity_type, $data){
      $entity = \Drupal::entityManager()->getStorage($entity_type)->create($data);
                    $entity->enforceIsNew(false);
                    $status = $entity->save();
                    return $entity->employee_id->value;
  }
  public static function getAllLanguagesByEmployeeId($id) {
        $database = \Drupal::database();
        $languages = $database->select('language','l')
                ->fields('l')
                ->condition('employee_id', $id)
                ->execute()
                ->fetchAll();
        return $languages;
    }
}