<?php

namespace Drupal\pim\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pim\PimManager;
use Drupal\pim\EmployeeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pim\entity\Employee;
use Drupal\pim\EducationManager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EducationController extends ControllerBase {

    protected $pimManager;
    protected $employeeManager;
    protected $entityTypeManager;
    protected $dbConnection;
    protected $educationManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, PimManager $pimManager, EmployeeManager $employeeManager, EducationManager $educationManager, Connection $connection) {
        $this->entityTypeManager = $entityTypeManager;
        $this->pimManager = $pimManager;
        $this->employeeManager = $employeeManager;
        $this->educationManager = $educationManager;
        $this->dbConnection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager'), $container->get('pim.manager'), $container->get('pim.employee_manager'), $container->get('pim.education.manager'), $container->get('database')
        );
    }

    public function saveEducation() {
        // print_r($_POST);die;
        try {
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();


            if (!isset($_POST['education'])) {
                return;
            }

            $data = json_decode($_POST['education'], true);
            $id = $data['id'];
            $isEdit = !empty($id);
            $permission = in_array('administrator', $roles) ? true : (!$isEdit ? $account->hasPermission('add education') : $account->hasPermission('edit education'));
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            $transaction = $this->dbConnection->startTransaction();
            try {
                if (isset($data['id']) && !empty($data['id'])) {

                    $employeeId = $this->educationManager->update('education_detail', $data);
                } else {

                    $employeeId = $this->educationManager->insert('education_detail', $data);
                }


                return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
            } catch (Exception $exc) {

                $transaction->rollback();
                return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
            }

            // $this->redirect('pim.default_controller_edit_employee_form');
        } catch (Exception $exc) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    public function deleteEducation() {

        try {

            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('delete education');
            if ($permission == false) {
                //throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
                return new JsonResponse(['status' => 'failure']);
            }

            if (!isset($_POST['ids']) && empty($_POST['ids'])) {
                return;
            }
            
            $ids = $_POST['ids'];
            $employeeId = $_POST['employee_id'];

            foreach ($ids as $key => $id) {
                $this->dbConnection->delete('education_detail')
                        ->condition('id', $id)
                        ->condition('employee_id', $employeeId)
                        ->execute();
            }

            return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

}
