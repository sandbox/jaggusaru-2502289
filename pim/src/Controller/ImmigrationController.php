<?php

namespace Drupal\pim\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pim\PimManager;
use Drupal\pim\EmployeeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pim\entity\Employee;

use Drupal\pim\ImmigrationManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ImmigrationController extends ControllerBase {

    protected $pimManager;
    protected $employeeManager;
    protected $entityTypeManager;
    protected $dbConnection;
    protected $immigrationManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, PimManager $pimManager, EmployeeManager $employeeManager, ImmigrationManager $immigrationManager, Connection $connection) {
        $this->entityTypeManager = $entityTypeManager;
        $this->pimManager = $pimManager;
        $this->employeeManager = $employeeManager;
        $this->immigrationManager = $immigrationManager;
        $this->dbConnection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager'), $container->get('pim.manager'), $container->get('pim.employee_manager'), $container->get('pim.immigration.manager'), $container->get('database')
        );
    }

    public function saveImmigration() {
        // print_r($_POST);die;
        try {
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();


            if (!isset($_POST['immigration'])) {
                return;
            }
            $data = json_decode($_POST['immigration'], true);
            $immigration = $_POST['immigraton'];
            $id = $dependent['id'];
            $isEdit = isset($immigration) && !empty($id);
            $permission = in_array('administrator', $roles) ? true : (!$isEdit ? $account->hasPermission('add immigration') : $account->hasPermission('edit immigration'));
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            $transaction = $this->dbConnection->startTransaction();
            try {
                if (isset($data['id']) && !empty($data['id'])) {

                    $employeeId = $this->immigrationManager->update('immigration', $data);
                } else {

                    $employeeId = $this->immigrationManager->insert('immigration', $data);
                }


                return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
            } catch (Exception $exc) {

                $transaction->rollback();
                return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
            }

            // $this->redirect('pim.default_controller_edit_employee_form');
        } catch (Exception $exc) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    public function deleteImmigration() {
        
        try {
           
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('delete dependent');
            if ($permission == false) {
               //throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
                return new JsonResponse(['status' => 'failure']);
            }
          
            if (!isset($_POST['ids']) && empty($_POST['ids'])) {
                return;
            }
            //print_r($_POST);die;
            $ids = $_POST['ids'];
            $employeeId = $_POST['employee_id'];
            
            foreach($ids as $key => $id)
             {
                    $this->dbConnection->delete('immigration')
                            ->condition('id', $id)
                            ->condition('employee_id', $employeeId)
                            ->execute();
                }
           
             return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

}
