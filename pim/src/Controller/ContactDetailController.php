<?php

namespace Drupal\pim\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Component\Utility\String;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\Role;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Drupal\pim\PimManager;
use Drupal\pim\EmployeeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pim\entity\Employee;
use Drupal\pim\ContactDetailManager;

/**
 * Class ContactDetailController.
 *
 * @package Drupal\pim\Controller
 */
class ContactDetailController extends ControllerBase {

    protected $pimManager;
    protected $contactDetailManager;
    protected $entityTypeManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $connection, PimManager $pimManager, ContactDetailManager $contactDetailManager) {
        $this->entityTypeManager = $entityTypeManager;
        $this->dbConnection = $connection;
        $this->pimManager = $pimManager;
        $this->contactDetailManager = $contactDetailManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager'), $container->get('database'), $container->get('pim.manager'), $container->get('pim.contact_detail.manager')
        );
    }

    /**
     * Addcontactdetailform.
     *
     * @return string
     *   Return Hello string.
     */
    public function addContactDetailForm() {
        return ['#theme' => 'employee_contact_detail'];
    }

    public function saveContactDetail() {
//print_r($_POST);die;

        try {
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('add contact');
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            if (!isset($_POST['contact'])) {
                return;
            }

            $data = json_decode($_POST['contact'], true);
            $transaction = $this->dbConnection->startTransaction();
            try {
                if (isset($data['id']) && !empty($data['id'])) {
                    $employeeId = $this->contactDetailManager->update('contact_detail', $data);
                } else {
                    $employeeId = $this->contactDetailManager->insert('contact_detail', $data);
                }
                return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
            } catch (Exception $exc) {

                $transaction->rollback();
                return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
            }

            $this->redirect('pim.default_controller_employee_contact_form');
            //$response = '';
        } catch (Exception $exc) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
        //return $response;
    }

}
