<?php

namespace Drupal\pim\Controller;

use Drupal\pim\FileUploadManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\JsonResponse;

class FileUploadController extends ControllerBase {

    public function uploadFile() {

        $account = \Drupal::currentUser();
        $roles = $account->getRoles();
        $permission = in_array('administrator', $roles) ? true : $account->hasPermission('upload file');
        if ($permission == false) {
            throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            //return new JsonResponse(['error' => $this->t('You do not have permission to upload file.')]);
            // return false;
        }
        $route_match = \Drupal::service('current_route_match');
        $employeeId = $route_match->getParameter('employee');
        try {
            if (isset($_FILES['attachment_file']) && !empty($file = $_FILES['attachment_file'])) {

                $name = $file['name'];
                $type = $file['type'];
                $tmp_name = $file['tmp_name'];
                switch ($_POST['attachment_type']) {
                    case 'employeeDetail':
                        $attachmentType = 'employeeDetail';
                        break;
                    case 'contactDetail':
                        $attachmentType = 'contactDetail';
                        break;
                    case 'emergencyContactDetail':
                        $attachmentType = 'emergencyContactDetail';
                        break;
                    case 'dependentDetail':
                        $attachmentType = 'dependentDetail';
                        break;
                    case 'immigrationDetail':
                        $attachmentType = 'immigrationDetail';
                        break;
                    case 'jobDetail':
                        $attachmentType = 'jobDetail';
                        break;
                    case 'salaryDetail':
                        $attachmentType = 'salaryDetail';
                        break;
                    case 'reportingDetail':
                        $attachmentType = 'reportingDetail';
                        break;
                    case 'qualificationDetail':
                        $attachmentType = 'qualificationDetail';
                        break;
                    default:
                        $attachmentType = 'employee';
                        break;
                }
                try {

                    $file = file_save_data($name, "public://" . $name, FILE_EXISTS_RENAME);
                    $file_usage = \Drupal::service('file.usage');
                    $file_usage->add($file, 'pim', $attachmentType, 1);
                    $file->save();

                    $files = \Drupal\pim\FileUploadManager::getAllFilesByUserId(1);

                    $html = $this->getAjaxFileGrid($files);

                    return new JsonResponse(['success' => $this->t('File Uploaded Successfully'), 'data' => $html]);
                } catch (Exception $ex) {
                    return new JsonResponse(['error' => $this->t('File could not be uploaded !')]);
                }
            } else {
                return new JsonResponse(['status' => $this->t('You must select file to upload.')]);
                return false;
            }
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    public function deleteFile() {

        try {

            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('delete file');
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            if (!isset($_POST['ids']) && empty($_POST['ids'])) {
                return;
            }

            $ids = $_POST['ids'];

            $storage_handler = \Drupal::entityManager()->getStorage('file');
            $entities = $storage_handler->loadMultiple($ids);
            $storage_handler->delete($entities);
            $files = \Drupal\pim\FileUploadManager::getAllFilesByUserId(1);

            $html = $this->getAjaxFileGrid($files);

            return new JsonResponse(['success' => $this->t('File Deleted Successfully'), 'data' => $html]);
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    public function getAjaxFileGrid($files) {
        $html = '';
        if (count($files) > 0) {
            foreach ($files as $file) {
                $html .= '<div class="row">';
                $html .= '<div class="col-sm-1"><input type="checkbox" class="file-checkbox" name="file_checkbox[]" value="' . $file->fid->value . '"></div>';
                $html .= '<div class="col-sm-1"><p>' . $file->filename->value . ' </p></div>';
                $html .= '<div class="col-sm-2"><a href="' . base_path() . ' pim/employee/viewPersonalDetail/' . $file->fid->value . ' ">' . $file->filename->value . '</a></div>';
                $html .= '<div class="col-sm-2"><p id="bb">' . $file->filesize->value . ' </p></div>';
                $html .= '<div class="col-sm-2">' . $file->filemime->value . '</div>';
                $html .= '<div class="col-sm-2">' . \Drupal::service('date.formatter')->format($file->created->value, 'short') . '</div>';
                $html .= '<div class="col-sm-2">' . $file->uid->target_id . '</div>';
                $html .= '</div>';
            }
        } else {
            $html .= '<div class="row rr">';
            $html .= '<div class="col-sm-12">';
            $html .= $this->t('No records found');
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    public function downloadFile($file) {
        $filename = base_path().'sites/default/files/'.$file; // of course find the exact filename....
        die($filename);
       // $filename = 'http://localhost/hrmssystem/sites/default/files/jagat.pdf';
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false); // required for certain browsers 
header('Content-Type: application/pdf');

header('Content-Disposition: attachment; filename="'. basename($filename) . '";');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($filename));

 file_get_contents($filename);

exit;
    }

}
