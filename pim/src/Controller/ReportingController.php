<?php

namespace Drupal\pim\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\pim\PimManager;
use Drupal\pim\EmployeeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pim\entity\Employee;
use Drupal\pim\ReportingManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ReportingController extends ControllerBase {

    protected $pimManager;
    protected $employeeManager;
    protected $entityTypeManager;
    protected $dbConnection;
    protected $reportingManager;

    public function __construct(EntityTypeManagerInterface $entityTypeManager, PimManager $pimManager, EmployeeManager $employeeManager, ReportingManager $reportingManager, Connection $connection) {
        $this->entityTypeManager = $entityTypeManager;
        $this->pimManager = $pimManager;
        $this->employeeManager = $employeeManager;
        $this->reportingManager = $reportingManager;
        $this->dbConnection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager'), $container->get('pim.manager'), $container->get('pim.employee_manager'), $container->get('pim.reporting.manager'), $container->get('database')
        );
    }

    public function saveReporting() {
        // print_r($_POST);die;
        try {
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();


            if (!isset($_POST['manager'])) {
                return;
            }
            $data = json_decode($_POST['manager'], true);
            $dependent = $_POST['manager'];
            $id = $datat['id'];
            $isEdit = isset($dependent) && !empty($id);
            $permission = in_array('administrator', $roles) ? true : (!$isEdit ? $account->hasPermission('add reporting') : $account->hasPermission('edit reporting'));
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            $transaction = $this->dbConnection->startTransaction();
            try {
                if (isset($data['id']) && !empty($data['id'])) {

                    $employeeId = $this->reportingManager->update('reporting_to', $data);
                } else {

                    $employeeId = $this->reportingManager->insert('reporting_to', $data);
                }


                return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
            } catch (Exception $exc) {

                $transaction->rollback();
                return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
            }

            // $this->redirect('pim.default_controller_edit_employee_form');
        } catch (Exception $exc) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    public function deleteReporting() {
 //print_r($_POST);die;
        try {

            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('delete reporting');
            if ($permission == false) {
                //throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
                return new JsonResponse(['status' => 'failure']);
            }

            if (!isset($_POST['ids']) && empty($_POST['ids'])) {
                return;
            }
           
            $ids = $_POST['ids'];
            $employeeId = $_POST['employee_id'];

            foreach ($ids as $key => $id) {
                $this->dbConnection->delete('reporting_to')
                        ->condition('id', $id)
                        ->condition('employee_id', $employeeId)
                        ->execute();
            }

            return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }
public function getReportingManagers(){
    $managers = $this->reportingManager->getEmployeeByJobtitile();
   
    $data = [];
    foreach($managers as $manager){
       
        $data[]= ['label' => $manager->first_name . ' ' . $manager->last_name, 'value'=> $manager->first_name . ' ' . $manager->last_name, 'desc' => $manager->id];
       
    }
  
   return new JsonResponse($data);
    
}
}
