<?php

namespace Drupal\pim\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\String;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user\Entity\Role;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Drupal\pim\PimManager;
use Drupal\pim\EmployeeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pim\entity\Employee;

/**
 * Class DefaultController.
 *
 * @package Drupal\pim\Controller
 */
class DefaultController extends ControllerBase {

    protected $pimManager;
    protected $employeeManager;
    protected $entityTypeManager;

    /**
     * Hello.
     *
     * @return string
     *   Return Hello string.
     */
    public function hello($name) {
        return [
            '#type' => 'markup',
            '#markup' => $this->t('Implement method: hello with parameter(s): $name'),
        ];
    }

    public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $connection, PimManager $pimManager, EmployeeManager $employeeManager) {
        $this->entityTypeManager = $entityTypeManager;
        $this->dbConnection = $connection;
        $this->pimManager = $pimManager;
        $this->employeeManager = $employeeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
                $container->get('entity_type.manager'), $container->get('database'), $container->get('pim.manager'), $container->get('pim.employee_manager')
        );
    }

    public function addEmployeeForm() {
        return ['#theme' => 'employee_add'];
    }

    public function getPersonalDetailForm($employee) {
        return ['#theme' => 'personal_detail'];
    }

    public function getContactForm($employee) {
        return ['#theme' => 'employee_contact'];
    }

    public function getDependentForm($employee) {
        return ['#theme' => 'employee_dependents'];
    }

    public function getEmergencyContactForm($employee) {
        return ['#theme' => 'employee_emergency_contact'];
    }

    public function getImmigrationForm($employee) {
        return ['#theme' => 'employee_immigration'];
    }

    public function getJobForm($employee) {
        return ['#theme' => 'employee_job'];
    }

    public function getQualificationForm($employee) {
        return ['#theme' => 'employee_qualification'];
    }

    public function getReportingForm($employee) {
        return ['#theme' => 'employee_reporting'];
    }

    public function getSalaryForm($employee) {
        return ['#theme' => 'employee_salary'];
    }

    public function hrmsAdminPage() {
        return ['#theme' => 'hrms_admin_content'];
    }

    public function viewEmployees() {
        return ['#theme' => 'employee_grid_view'];
    }

    public function getEmployeeIds() {
        $managers = $this->employeeManager->getAllEmployees();

        $data = [];
        foreach ($managers as $manager) {

            $data[] = ['label' => $manager->first_name . ' ' . $manager->last_name . '(' . $manager->id . ')', 'value' => $manager->first_name . ' ' . $manager->last_name . '(' . $manager->id . ')', 'desc' => $manager->id];
        }

        return new JsonResponse($data);
    }

    public function getAllSupervisors() {
        $managers = $this->employeeManager->getAllSuperVisors();
        $data = [];
        foreach ($managers as $manager) {

            $data[] = ['label' => $manager->first_name . ' ' . $manager->last_name, 'value' => $manager->first_name . ' ' . $manager->last_name, 'desc' => $manager->id];
        }
        return new JsonResponse($data);
    }

    public function getEmployeeBySearchCriteria() {
       
        $data = json_decode($_POST['searchEmp'], true);
        
        $employees = EmployeeManager::getEmployeeBySearchCriteria($data['id']);
        
            $html = '';
            if(count($employees)> 1){
                foreach ($employees as $employee) {
                $html .= '<div class="row rr">';

                $html .= '<div class="col-sm-1"><input type="checkbox" class="employee-checkbox" name="employee_checkbox[]" value="' . $employee->id . '"></div>';
                $html .= '<div class="col-sm-1"><p id="idofemp">' . $employee->id . ' </p></div>';
                $html .= '<div class="col-sm-2"><a href="' . base_path() . ' pim/employee/viewPersonalDetail/' . $employee->id . ' ">' . $employee->first_name . ' ' . $employee->last_name . '</a></div>';

                $html .= '<div class="col-sm-2"><p id="bb">' . $employee->first_name . ' </p></div>';
               
                $html .= '<div class="col-sm-2">' . $employee->first_name . '</div>';
                $html .= '<div class="col-sm-2">' . $employee->first_name . '</div>';
                $html .= '<div class="col-sm-2">' . $employee->first_name . '</div>';
                $html .= '</div>';
            }
            }elseif (count($employees == 1)){
                 $html .= '<div class="row rr">';

                $html .= '<div class="col-sm-1"><input type="checkbox" class="employee-checkbox" name="employee_checkbox[]" value="' . $employees->id . '"></div>';
                $html .= '<div class="col-sm-1"><p id="idofemp">' . $employees->id . ' </p></div>';
                $html .= '<div class="col-sm-2"><a href="' . base_path() . ' pim/employee/viewPersonalDetail/' . $employees->id . ' ">' . $employees->first_name . ' ' . $employees->last_name . '</a></div>';

                $html .= '<div class="col-sm-2"><p id="bb">' . $employees->first_name . ' </p></div>';
               
                $html .= '<div class="col-sm-2">' . $employees->first_name . '</div>';
                $html .= '<div class="col-sm-2">' . $employees->first_name . '</div>';
                $html .= '<div class="col-sm-2">' . $employees ->first_name . '</div>';
                $html .= '</div>';
            }
            else{
                $html .= '<div class="row rr">';
                $html .= '<div class="col-sm-12">';
                $html .= $this->t('No records found');
                $html .= '</div>';
                $html .= '</div>';
            }

        return new JsonResponse($html);
    }

    public function searchEmployee() {
        print_r($_POST);
        die;
    }

    public function saveEmployee() {


        try {
            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('add employee');
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }

            if (!isset($_POST['employee'])) {
                return;
            }

            $employeeData = json_decode($_POST['employee'], true);



            $transaction = $this->dbConnection->startTransaction();
            try {
                if (isset($employeeData['id']) && !empty($employeeData['id'])) {
                    $employeeId = $this->employeeManager->update('employee', $employeeData);
                } else {
                    $employeeId = $this->employeeManager->insert('employee', $employeeData);
                }


                if (isset($_POST['account']) && !empty($_POST['account'])) {

                    $accountData = json_decode($_POST['account'], true);
                    $userId = $this->employeeManager->insert('user', $accountData);
                }

                return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
            } catch (Exception $exc) {

                $transaction->rollback();
                return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
            }

            $this->redirect('pim.default_controller_edit_employee_form');
            //$response = '';
        } catch (Exception $exc) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
        //return $response;
    }

    public function terminateEmployee() {
        try {
            //print_r($_POST);die;

            $account = \Drupal::currentUser();
            $roles = $account->getRoles();
            $permission = in_array('administrator', $roles) ? true : $account->hasPermission('terminate employee');
            if ($permission == false) {
                throw new AccessDeniedException($this->t('You do not have required privileges for this task'));
            }
            //print_r($_POST);die;
            if (!isset($_POST['id'])) {
                return;
            }

            $employeeId = $this->employeeManager->update('employee', $_POST);
            return new JsonResponse(['status' => 'success', 'employee' => $employeeId]);
        } catch (Exception $ex) {
            return new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
    }

    /**
     * [AJAX] This method checks whether the username already exists in the database. 
     * 
     * @param string $_POST['username'] Record's username to validate.
     * @param bool $_POST['record_exists'] Whether the record already exists in database.
     */
    public function validateUsername() {
        try {
            // We will only use the function in the admins_model because it is sufficient 
            // for the rest user types for now (providers, secretaries).
            // print_r($_POST);die;
            $is_valid = $this->pimManager->validate_username($_POST['username']);
            //echo $is_valid ? true : false;die;
            $response = new JsonResponse($is_valid);
            //echo json_encode($is_valid);
        } catch (Exception $exc) {
            $response = new JsonResponse(['exceptions' => array(exceptionToJavaScript($exc))]);
        }
        return $response;
    }

}
