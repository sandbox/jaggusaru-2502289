<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Dependents entities.
 *
 * @ingroup pim
 */
interface DependentsInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Dependents name.
   *
   * @return string
   *   Name of the Dependents.
   */
  public function getName();

  /**
   * Sets the Dependents name.
   *
   * @param string $name
   *   The Dependents name.
   *
   * @return \Drupal\pim\DependentsInterface
   *   The called Dependents entity.
   */
  public function setName($name);

  /**
   * Gets the Dependents creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Dependents.
   */
  public function getCreatedTime();

  /**
   * Sets the Dependents creation timestamp.
   *
   * @param int $timestamp
   *   The Dependents creation timestamp.
   *
   * @return \Drupal\pim\DependentsInterface
   *   The called Dependents entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Dependents published status indicator.
   *
   * Unpublished Dependents are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Dependents is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Dependents.
   *
   * @param bool $published
   *   TRUE to set this Dependents to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\DependentsInterface
   *   The called Dependents entity.
   */
  public function setPublished($published);

}
