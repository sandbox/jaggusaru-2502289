<?php
namespace Drupal\pim;
use Drupal\pim\FileUploadInterface;
use Drupal\file\Entity\File;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FileUploadManager implements FileUploadInterface{
    
  public static function getAllFilesByUserId($id) {
      
      $query = \Drupal::entityQuery('file')
              ->condition('uid', $id);
       $nids = $query->execute();
        return File::loadMultiple($nids);
         
    }
}