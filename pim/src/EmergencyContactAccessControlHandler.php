<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Emergency contact entity.
 *
 * @see \Drupal\pim\Entity\EmergencyContact.
 */
class EmergencyContactAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\EmergencyContactInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished emergency contact entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published emergency contact entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit emergency contact entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete emergency contact entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add emergency contact entities');
  }

}
