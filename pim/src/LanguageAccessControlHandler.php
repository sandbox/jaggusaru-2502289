<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Language entity.
 *
 * @see \Drupal\pim\Entity\Language.
 */
class LanguageAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\LanguageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished language entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published language entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit language entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete language entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add language entities');
  }

}
