<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Contact detail entities.
 *
 * @ingroup pim
 */
interface ContactDetailInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Contact detail name.
   *
   * @return string
   *   Name of the Contact detail.
   */
  public function getName();

  /**
   * Sets the Contact detail name.
   *
   * @param string $name
   *   The Contact detail name.
   *
   * @return \Drupal\pim\ContactDetailInterface
   *   The called Contact detail entity.
   */
  public function setName($name);

  /**
   * Gets the Contact detail creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Contact detail.
   */
  public function getCreatedTime();

  /**
   * Sets the Contact detail creation timestamp.
   *
   * @param int $timestamp
   *   The Contact detail creation timestamp.
   *
   * @return \Drupal\pim\ContactDetailInterface
   *   The called Contact detail entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Contact detail published status indicator.
   *
   * Unpublished Contact detail are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Contact detail is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Contact detail.
   *
   * @param bool $published
   *   TRUE to set this Contact detail to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\ContactDetailInterface
   *   The called Contact detail entity.
   */
  public function setPublished($published);

}
