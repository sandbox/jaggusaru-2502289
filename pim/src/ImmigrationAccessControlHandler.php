<?php

namespace Drupal\pim;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Immigration entity.
 *
 * @see \Drupal\pim\Entity\Immigration.
 */
class ImmigrationAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pim\ImmigrationInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished immigration entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published immigration entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit immigration entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete immigration entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add immigration entities');
  }

}
