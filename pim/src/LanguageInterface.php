<?php

namespace Drupal\pim;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Language entities.
 *
 * @ingroup pim
 */
interface LanguageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Language name.
   *
   * @return string
   *   Name of the Language.
   */
  public function getName();

  /**
   * Sets the Language name.
   *
   * @param string $name
   *   The Language name.
   *
   * @return \Drupal\pim\LanguageInterface
   *   The called Language entity.
   */
  public function setName($name);

  /**
   * Gets the Language creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Language.
   */
  public function getCreatedTime();

  /**
   * Sets the Language creation timestamp.
   *
   * @param int $timestamp
   *   The Language creation timestamp.
   *
   * @return \Drupal\pim\LanguageInterface
   *   The called Language entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Language published status indicator.
   *
   * Unpublished Language are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Language is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Language.
   *
   * @param bool $published
   *   TRUE to set this Language to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pim\LanguageInterface
   *   The called Language entity.
   */
  public function setPublished($published);

}
