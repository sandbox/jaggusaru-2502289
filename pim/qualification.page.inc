<?php

/**
 * @file
 * Contains qualification.page.inc.
 *
 * Page callback for Qualification entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Qualification templates.
 *
 * Default template: qualification.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_qualification(array &$variables) {
  // Fetch Qualification Entity Object.
  $qualification = $variables['elements']['#qualification'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
