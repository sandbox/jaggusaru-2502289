<?php

/**
 * @file
 * Contains language.page.inc.
 *
 * Page callback for Language entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Language templates.
 *
 * Default template: language.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_language(array &$variables) {
  // Fetch Language Entity Object.
  $language = $variables['elements']['#language'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
