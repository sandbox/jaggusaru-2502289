<?php

/**
 * @file
 * Contains reporting_to.page.inc.
 *
 * Page callback for Reporting to entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Reporting to templates.
 *
 * Default template: reporting_to.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_reporting_to(array &$variables) {
  // Fetch ReportingTo Entity Object.
  $reporting_to = $variables['elements']['#reporting_to'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
