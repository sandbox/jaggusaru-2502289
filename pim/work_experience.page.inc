<?php

/**
 * @file
 * Contains work_experience.page.inc.
 *
 * Page callback for Work experience entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Work experience templates.
 *
 * Default template: work_experience.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_work_experience(array &$variables) {
  // Fetch WorkExperience Entity Object.
  $work_experience = $variables['elements']['#work_experience'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
