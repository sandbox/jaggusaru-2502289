<?php

/**
 * @file
 * Contains immigration.page.inc.
 *
 * Page callback for Immigration entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Immigration templates.
 *
 * Default template: immigration.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_immigration(array &$variables) {
  // Fetch Immigration Entity Object.
  $immigration = $variables['elements']['#immigration'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
