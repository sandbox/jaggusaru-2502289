<?php

/**
 * @file
 * Contains contact_detail.page.inc.
 *
 * Page callback for Contact detail entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Contact detail templates.
 *
 * Default template: contact_detail.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_contact_detail(array &$variables) {
  // Fetch ContactDetail Entity Object.
  $contact_detail = $variables['elements']['#contact_detail'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
