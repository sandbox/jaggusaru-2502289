/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addEducation = {
        attach: function (context, settings) {
            $('#save-education').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Education = {
                    'id': $('#id').val(),
                    'level': $('#level').val(),
                    'institute': $('#institute').val(),
                    'start_date': $('#start-date').val(),
                    'end_date': $('#end-date').val(),
                    'year': $('#specialized-year').val(),
                   'specialization' : $('#specialization').val(),
                   'gpa_score' : $('#gpa-score').val(),
                    
                    'employee_id': $('#employee-id').val(),
                };


                postData['education'] = JSON.stringify(Education);


                var postUrl = drupalSettings.path.baseUrl + 'pim/education/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Education Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editEducation = {
        attach: function (context, settings) {
            $('a#education').click(function () {
               
                var index = $(this).closest('tr').find('input').val();
                var id = index;
               console.log(index);
                var level = $('#level-' + index).text();
                var institute = $('#institute' + index).text();
                var year = $('#specialized-year-'+ index).text();
                var start_date = $('#start-date-' + index).text();
                var end_date = $('#end-date-' + index).text();
                var specialization = $('#specialization-' + index).text();
                var gpa_score = $('#gpa-score-' + index).text();
                
                $('#id').val(id);
               console.log(level);
               console.log(gpa_score);
                $('#level').val(level);
                $('#institute').val(institute);
                $('#start-date').val(start_date);
                $('#specialized-year').val(year);
                $('#end-date').val(end_date);
                $('#specialization').val(specialization);
                $('#gpa-score').val(gpa_score);


                $('#education-panel').find('panel-title').html(Drupal.t('Edit Education'));
                $('#education-panel').show();
                $('#add-education').hide();
                $('#delete-education').hide();

            });
        }
    };
    Drupal.behaviors.deleteEducation = {
        attach: function (context, settings) {
            $('#delete-education').on('click', function () {
                var checkboxes = new Array();

                $('.education-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

                // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/education/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Education Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeEducationCheckboxChecked = {
        attach: function (context, settings) {

            /*
             
             $("#check_all_emergency_contact").click(function(){
             if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
             $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
             } else {
             $('#delete-emergency_contact-checkbox').removeAttr('checked');
             }
             */
            $("#check-all-education").on('click', function () {
                
                if ($('#check-all-education').is(':checked') == true) {


                    $(".education-checkbox").attr('checked', 'checked');
                } else {
                    
                    $(".education-checkbox").removeAttr('checked');
                }
                
                if ($('.education-checkbox:checkbox:checked').length > 0) {
                    $('#delete-education').removeAttr('disabled');
                } else {
                    $('#delete-education').attr('disabled', 'disabled');
                }
            });
            $('.education-checkbox').click(function () {
                if ($('.education-checkbox:checkbox:checked').length > 0) {
                    $('#delete-education').removeAttr('disabled');
                } else {
                    $('#delete-education').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);