/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addSalary = {
        attach: function (context, settings) {
            $('#save-salary').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Salary = {
                    'id': $('#id').val(),
                    
                    'pay_grade': $('#pay-grade').val(),
                    'salary_component': $('#salary-component').val(),
                    'pay_frequency': $('#pay_frequency option:selected').val(),
                    'currency': $('#currency').val(),
                    'amount': $('#amount').val(),
                    'comment': $('#comment').val(),
                    'account_number': $('#account-number').val(),
                    'account_type': $('#account-type option:selected').val(),
                    'routing_number': $('#routing-number').val(),
                    'direct_deposit_amount': $('#direct-deposit-amount').val(),
                    'employee_id': $('#employee-id').val(),
                };


                postData['salary'] = JSON.stringify(Salary);


                var postUrl = drupalSettings.path.baseUrl + 'pim/salary/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Salary Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewSalaryDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editSalary = {
        attach: function (context, settings) {
            $('a#pay-grade').click(function () {
                //console.log();
                $('div#direct-deposit-detail').addClass('in');
                var index = $(this).closest('tr').find('input').val();
                var id = index;
                // console.log(id);
                var pay_grade = $('#pay-grade-' + index).text();
                var salary_component = $('#salary-component-' + index).text();

                var pay_frequency = $('#pay-frequency-' + index).text();
                $('#pay_frequency option').each(function(i, v){
                    if($.trim(pay_frequency) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                   
                });
               
                var currency = $('#currency-' + index).text();
               
                var amount = $('#amount-' + index).text();
                
                var comment = $('#comment-' + index).text();
                var account_number = $('#account-number-' + index).text();
                var account_type = $('#account-type-' + index).text();
                $('#currency option').each(function(i,v){
                    if($.trim(currency) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                });
                var routing_number = $('#routing-number-' + index).text();
                var direct_deposit_amount = $('#direct-deposit-amount-' + index).text();
                    $('#id').val(id);
                   // $('#employee-id').val(),
                    $('#pay-grade').val(pay_grade);
                    $('#salary-component').val(salary_component);
                    $('#pay-frequency').val(pay_frequency);
                     $('#currency option').each(function(i,v){
                    if($.trim(currency) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                });
                   // $('#currency').val(currency),
                    $('#amount').val(amount);
                    $('#comment').val(comment);
                    $('#account-number').val(account_number);
                    $('#account-type option').each(function(i,v){
                     if($.trim(account_type) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                });
                   
                    $('#routing-number').val(routing_number),
                    $('#direct-deposit-amount').val(direct_deposit_amount);

                
                $('#salary-panel').find('panel-title').html('Edit Salary');
                $('#salary-panel').show();
                $('#add-salary').hide();
                $('#delete-salary').hide();

            });
        }
    };
    Drupal.behaviors.deleteSalary = {
        attach: function (context, settings) {
            $('#delete-salary').on('click', function () {
                var checkboxes = new Array();

                $('.checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

                // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/salary/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Salary Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewSalaryDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeSalaryCheckboxChecked = {
        attach: function (context, settings) {

            /*
             
             $("#check_all_emergency_contact").click(function(){
             if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
             $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
             } else {
             $('#delete-emergency_contact-checkbox').removeAttr('checked');
             }
             */
            $("#check-all").on('click', function () {
                console.log($("#check-all:checked").attr('value'));
                console.log($('#check-all').is(':checked'));
                if ($('#check-all').is(':checked') == true) {


                    $(".checkbox").attr('checked', 'checked');
                } else {
                    console.log('asdfsdaasdfasdfsadf');
                    $(".checkbox").removeAttr('checked');
                }
                /*
                 
                 $('#check-all').bind('click',function () {
                 
                 if ($(this).is(':checked')) {
                 $('.checkbox').attr('checked', 'checked');
                 //console.log($('.checkbox:checkbox:checked').length);
                 } else {
                 $('.checkbox').removeAttr('checked');
                 }*/
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-salary').removeAttr('disabled');
                } else {
                    $('#delete-salary').attr('disabled', 'disabled');
                }
            });
            $('.checkbox').click(function () {
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-salary').removeAttr('disabled');
                } else {
                    $('#delete-salary').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);