/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addEmergencyContact = {
        attach: function (context, settings) {
            $('#save-emergency-contact').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Contact = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'name': $('#name').val(),
                    'relationship': $('#relationship').val(),
                    'home_phone': $('#home-phone').val(),
                    'mobile': $('#mobile').val(),
                    'work_phone': $('#work-phone').val(),
                    'work_email': $('#work-email').val(),
                    'other_email': $('#other-email').val(),
                };


                postData['emergency_contact'] = JSON.stringify(Contact);


                var postUrl = drupalSettings.path.baseUrl + 'pim/emergencyContact/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Contact Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewEmergencyContact/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editEmergencyContact = {
        attach: function (context, settings) {
            $('a').click(function () {
                //console.log();
                var index = $(this).closest('tr').find('input').val();
                var id = index;
                // console.log(id);
                var name = $('#name-' + index).find('a').text();
                var relationship = $('#relationship-' + index).text();
                var work_phone = $('#work-phone-' + index).text();
                var home_phone = $('#home-phone-' + index).text();
                var mobile = $('#mobile-' + index).text();
                $('#id').val(id);
                //$('#employee-id').val(name);
                $('#name').val(name);
                $('#relationship').val(relationship);
                $('#home-phone').val(home_phone);
                $('#mobile').val(mobile);
                $('#work-phone').val(work_phone);
                $('#emergency-contact-panel').find('panel-title').html('Edit Emergency Contact');
                $('#emergency-contact-panel').show();
                $('#add-emergency-contact').hide();
                $('#delete-emergency-contact').hide();

            });
        }
    };
    Drupal.behaviors.deleteEmergencyContact = {
        attach: function (context, settings) {
            $('#delete-emergency-contact').on('click', function(){
                 var checkboxes = new Array();
            
            $('.checkbox:checked').each(function(){
               checkboxes.push($(this).val());
                //console.log('in');
            });
            var postData = {};
             postData['ids'] = checkboxes;
            postData['employee_id'] = $('#employee-id').val();
                
           // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/emergencyContact/delete';

                $.post(postUrl, postData, function (response) {
                   // console.log(response);return false;
                           // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Contact Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewEmergencyContact/' + response.employee;
                   

                }, 'json');
            });
           
        }
    };
    Drupal.behaviors.makeCheckboxChecked = {
        attach: function (context, settings) {
            
          /*

        $("#check_all_emergency_contact").click(function(){
            if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
                $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
            } else {
                $('#delete-emergency_contact-checkbox').removeAttr('checked');
            }
            */
           
       

            $('#check-all').bind('click',function () {

                if ($(this).is(':checked')) {
                    $('.checkbox').attr('checked', 'checked');
                    //console.log($('.checkbox:checkbox:checked').length);
                } else {
                    $('.checkbox').removeAttr('checked');
                }
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-emergency-contact').removeAttr('disabled');
            } else {
                $('#delete-emergency-contact').attr('disabled', 'disabled');
            }
            });
            $('.checkbox').click(function(){
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-emergency-contact').removeAttr('disabled');
            } else {
                $('#delete-emergency-contact').attr('disabled', 'disabled');
            }
            });
            
             
        }
    };
})(jQuery, window, Drupal);