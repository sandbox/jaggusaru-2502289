/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addImmigration = {
        attach: function (context, settings) {
            $('#save-immigration').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Immigration = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'document': $('#document').val(),
                    'number': $('#number').val(),
                    'issued_date': $('#issued-date').val(),
                    'expiry_date': $('#expiry-date').val(),
                    'elligible_status': $('#elligible-status').val(),
                     'issued_by': $('#issued-by').val(),
                      'eligible_review_date': $('#eligible-review-date').val(),
                       'comment': $('#comment').val(),
                    
                };


                postData['immigration'] = JSON.stringify(Immigration);


                var postUrl = drupalSettings.path.baseUrl + 'pim/immigration/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Immigration Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewImmigrationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);
                  

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editImmigration = {
        attach: function (context, settings) {
            $('a#immigration-document').click(function () {
                //console.log();
                var index = $(this).closest('tr').find('input').val();
                var id = index;
              //console.log(id);
                //var document = $('#document-' + index).find('a').text();
                var number = $('#number-' + index).text();
                var issued_date = $('#issued-date-' + index).text();
                var issued_by = $('#issued-by-' + index).text();
                var expiry_date = $('#expiry-date-' + index).text();
                var comment = $('#comment-' + index).text();
                var eligible_status = $('#eligible-status-' + index).text();
                var eligible_review_date = $('#eligible-review-date-' + index).text();
                //var comment = $('#comment-' + index).text();
               
                $('#id').val(id);
                //$('#employee-id').val(name);
                ($.trim(document) == 'passport')? $('input[value="passport"]').attr('checked', 'checked') : $('input[value="visa"]').attr('checked', 'checked');
                    
                
                var document = $('input[type="radio"]:checked').val();
                console.log(document);
                $('#number').val(number);
                $('#issued-date').val(issued_date);
                $('#issued-by').val(issued_by);
                $('#expiry-date').val(expiry_date);
                
                $('#eligible-status').val(eligible_status);
                $('#eligible-review-date').val(eligible_review_date);
                $('#comment').val(comment);
                $('#immigration-panel').find('panel-title').html('Edit Immigration');
                $('#immigration-panel').show();
                $('#add-immigration').hide();
                $('#delete-immigration').hide();

            });
        }
    };
    Drupal.behaviors.deleteImmigration = {
        attach: function (context, settings) {
            $('#delete-immigration').on('click', function(){
                 var checkboxes = new Array();
            
            $('.checkbox:checked').each(function(){
               checkboxes.push($(this).val());
                
            });
            var postData = {};
             postData['ids'] = checkboxes;
            postData['employee_id'] = $('#employee-id').val();
                
          
                var postUrl = drupalSettings.path.baseUrl + 'pim/immigration/delete';

                $.post(postUrl, postData, function (response) {
                   // console.log(response);return false;
                           // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Immiration Deleted'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewImmigrationDetails/' + response.employee;
                   

                }, 'json');
            });
           
        }
    };
    Drupal.behaviors.makeCheckboxCheckedImmigration = {
        attach: function (context, settings) {
            
          /*

        $("#check_all_emergency_contact").click(function(){
            if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
                $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
            } else {
                $('#delete-emergency_contact-checkbox').removeAttr('checked');
            }
            */
           $("#check-all").on('click',function(){
                
               if($('#check-all').is(':checked') == true){
                   
                
                $(".checkbox").attr('checked', 'checked');
            } else {
               
                $(".checkbox").removeAttr('checked');
            }
       /*

            $('#check-all').bind('click',function () {

                if ($(this).is(':checked')) {
                    $('.checkbox').attr('checked', 'checked');
                    //console.log($('.checkbox:checkbox:checked').length);
                } else {
                    $('.checkbox').removeAttr('checked');
                }*/
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-immigration').removeAttr('disabled');
            } else {
                $('#delete-immigration').attr('disabled', 'disabled');
            }
            });
            $('.checkbox').click(function(){
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-immigration').removeAttr('disabled');
            } else {
                $('#delete-immigration').attr('disabled', 'disabled');
            }
            });
            
             
        }
    };
})(jQuery, window, Drupal);