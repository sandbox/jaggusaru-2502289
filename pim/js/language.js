/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addLanguage = {
        attach: function (context, settings) {
            $('#save-language').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Language = {
                    'id': $('#language-id').val(),
                    'language': $('#language option:selected').val(),
                    'fluency': $('#fluency').val(),
                    'competency': $('#competency').val(),
                    'comment': $('#language-comment').val(),
                    'employee_id': $('#employee-id').val(),
                };


                postData['language'] = JSON.stringify(Language);


                var postUrl = drupalSettings.path.baseUrl + 'pim/language/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Language Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editLanguage = {
        attach: function (context, settings) {
            $('a#language').click(function () {

                var index = $(this).closest('tr').find('input').val();
                var id = index;
                console.log(index);
                var language = $('#language-' + index).text();
                var fluency = $('#fluency-' + index).text();
                var competency = $('#competency-' + index).text();
                 var comment = $('#language-comment-' + index).text();

                $('#language-id').val(id);
               
                $('#language option').each(function(i,v){
                    if($.trim(language) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                  
                });
                $('#fluency option').each(function(i,v){
                    if($.trim(fluency) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                  
                });
                $('#competency option').each(function(i,v){
                    if($.trim(competency) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                  
                });
               // $('#fluency').val(fluency);
               //   $('#competency').val(competency);
                $('#language-comment').val(comment);
               


                $('#language-panel').find('panel-title').html(Drupal.t('Edit Language'));
                $('#language-panel').show();
                $('#add-language').hide();
                $('#delete-language').hide();

            });
        }
    };
    Drupal.behaviors.deleteLanguage = {
        attach: function (context, settings) {
            $('#delete-language').on('click', function () {
                var checkboxes = new Array();

                $('.language-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

               
                var postUrl = drupalSettings.path.baseUrl + 'pim/language/delete';

                $.post(postUrl, postData, function (response) {
                   
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Language Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeLanguageCheckboxChecked = {
        attach: function (context, settings) {

           
            $("#check-all-languages").on('click', function () {

                if ($('#check-all-languages').is(':checked') == true) {


                    $(".language-checkbox").attr('checked', 'checked');
                } else {

                    $(".language-checkbox").removeAttr('checked');
                }

                if ($('.language-checkbox:checkbox:checked').length > 0) {
                    $('#delete-language').removeAttr('disabled');
                } else {
                    $('#delete-language').attr('disabled', 'disabled');
                }
            });
            $('.language-checkbox').click(function () {
                if ($('.language-checkbox:checkbox:checked').length > 0) {
                    $('#delete-language').removeAttr('disabled');
                } else {
                    $('#delete-language').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);