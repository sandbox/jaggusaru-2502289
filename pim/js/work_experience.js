/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addWorkExperience = {
        attach: function (context, settings) {
            $('#save-work-experience').click(function (event) {
                var postData = {};
                event.preventDefault();
                var WorkExperience = {
                    'id': $('#id').val(),
                    'company': $('#company').val(),
                    'job_title': $('#job-title').val(),
                    'from_date': $('#from-date').val(),
                    'to_date': $('#to-date').val(),
                    'comment': $('#work-exp-comment').val(),
                    'employee_id': $('#employee-id').val(),
                };


                postData['work_exp'] = JSON.stringify(WorkExperience);


                var postUrl = drupalSettings.path.baseUrl + 'pim/workExperience/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Work Experience Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editWorkExperience = {
        attach: function (context, settings) {
            $('a#work-exp').click(function () {
                console.log('sadfsdf');
                // $('div#direct-deposit-detail').addClass('in');
                var index = $(this).closest('tr').find('input').val();
                var id = index;
                // console.log(id);
                var company = $('#company-' + index).text();
                var job_title = $('#job-title-' + index).text();

                var to_date = $('#to-date-' + index).text();
                var from_date = $('#from-date-' + index).text();
                var comment = $('#work-exp-comment-' + index).text();
                
                $('#id').val(id);
               
                $('#company').val(company);
                $('#job-title').val(job_title);
                $('#to-date').val(to_date);
                
                $('#from-date').val(from_date);
                $('#work-exp-comment').val(comment);
                


                $('#work-experience-panel').find('panel-title').html(Drupal.t('Edit Work Experience'));
                $('#work-experience-panel').show();
                $('#add-work-experience').hide();
                $('#delete-work-experience').hide();

            });
        }
    };
    Drupal.behaviors.deleteWorkExperience = {
        attach: function (context, settings) {
            $('#delete-work-experience').on('click', function () {
                var checkboxes = new Array();

                $('.work-exp-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

                // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/WorkExperience/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Work Experience Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeWorkExpCheckboxChecked = {
        attach: function (context, settings) {

            /*
             
             $("#check_all_emergency_contact").click(function(){
             if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
             $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
             } else {
             $('#delete-emergency_contact-checkbox').removeAttr('checked');
             }
             */
            $("#check-all-work-exp").on('click', function () {
                
                if ($('#check-all-work-exp').is(':checked') == true) {


                    $(".work-exp-checkbox").attr('checked', 'checked');
                } else {
                    
                    $(".work-exp-checkbox").removeAttr('checked');
                }
                
                if ($('.work-exp-checkbox:checkbox:checked').length > 0) {
                    $('#delete-work-experience').removeAttr('disabled');
                } else {
                    $('#delete-work-experience').attr('disabled', 'disabled');
                }
            });
            $('.work-exp-checkbox').click(function () {
                if ($('.work-exp-checkbox:checkbox:checked').length > 0) {
                    $('#delete-work-experience').removeAttr('disabled');
                } else {
                    $('#delete-work-experience').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);