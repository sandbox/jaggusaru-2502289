/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addReporting = {
        attach: function (context, settings) {
            $('#save-reporting').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Manager = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'reporting_manager_id': $('#reporting-manager-id').val(),
                    'reporting_method': $('#reporting-method').val(),
                    'reporting_manager_type': $('#reporting-manager-type').val(),
                };


                postData['manager'] = JSON.stringify(Manager);


                var postUrl = drupalSettings.path.baseUrl + 'pim/reporting/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Reporting Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewReportingDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };

    Drupal.behaviors.editReporting = {
        attach: function (context, settings) {
            $('a#managers').click(function () {
                console.log('asdfasdf');
                var index = $(this).closest('tr').find('input').val();
                var id = index;
                console.log(id);
                var reporting_manager = $('#reporting-manager-' + index).find('a').text();
                var reporting_method = $('#reporting-method-' + index).text();
                // var reporting_manager_type = $('#reporting-manager-type-' + index).text();
                console.log(reporting_manager);
                $('#id').val(id);
                //   $('#employee-id').val(reporting_method);
                $('#reporting-manager').val(reporting_manager);
                //$('#reporting-method').val(reporting_method);
                $('#reporting-method option').each(function (i, v) {
                    if ($.trim(reporting_method) == $.trim($(v).val())) {
                        $(v).attr('selected', 'selected');
                    }
                });
                $('#reporting-manager-type').val('manager');

                $('#reporting-panel').find('panel-title').html('Edit Manager');
                $('#reporting-panel').show();
                $('#add-reporting').hide();
                $('#delete-reporting').hide();

            });

            // ################# Subordinate link click #############3
            $('a#subordinates').click(function () {
                
                var index = $(this).closest('tr').find('input').val();
                var id = index;
               
                var reporting_manager = $('#reporting-manager-' + index).find('a#subordinates').text();
                var reporting_method = $('#reporting-method-' + index).text();
                // var reporting_manager_type = $('#reporting-manager-type-' + index).text();
                //console.log(reporting_method);
                $('#reporting-method option').each(function (i, v) {
                    if ($.trim(reporting_method) == $.trim($(v).val())) {
                        $(v).attr('selected', 'selected');
                    }
                });
                $('#id').val(id);
                //   $('#employee-id').val(reporting_method);
                $('#reporting-manager').val(reporting_manager);
                $('#reporting-method').val(reporting_method);
                $('#reporting-manager-type').val('subordinate');

                $('#reporting-panel').find('panel-title').html('Edit Subordinate');
                $('#reporting-panel').show();
                $('#add-reporting').hide();
                $('#delete-reporting').hide();

            });
        }
    };
    Drupal.behaviors.deleteReporting = {
        attach: function (context, settings) {
            $('#delete-reporting').on('click', function () {
                var checkboxes = new Array();

                $('.checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

                // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/reporting/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('reporting Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewReportingDetails/' + response.employee;


                }, 'json');
            });
            
            $('#delete-subordinate').on('click', function () {
                var checkboxes = new Array();

                $('.subordinate-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

              
                var postUrl = drupalSettings.path.baseUrl + 'pim/reporting/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Subordinate deleted'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewReportingDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeReportingCheckboxChecked = {
        attach: function (context, settings) {

            /*
             
             $("#check_all_emergency_contact").click(function(){
             if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
             $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
             } else {
             $('#delete-emergency_contact-checkbox').removeAttr('checked');
             }
             */
            $("#check-all").on('click', function () {
                // console.log($("#check-all:checked").attr('value'));
                // console.log($('#check-all').is(':checked'));
                if ($('#check-all').is(':checked') == true) {


                    $(".checkbox").attr('checked', 'checked');
                } else {
                    // console.log('asdfsdaasdfasdfsadf');
                    $(".checkbox").removeAttr('checked');
                }
                /*
                 
                 $('#check-all').bind('click',function () {
                 
                 if ($(this).is(':checked')) {
                 $('.checkbox').attr('checked', 'checked');
                 //console.log($('.checkbox:checkbox:checked').length);
                 } else {
                 $('.checkbox').removeAttr('checked');
                 }*/
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-reporting').removeAttr('disabled');
                } else {
                    $('#delete-reporting').attr('disabled', 'disabled');
                }
            });
            $('.checkbox').click(function () {
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-reporting').removeAttr('disabled');
                } else {
                    $('#delete-reporting').attr('disabled', 'disabled');
                }
            });
            // ######################### Subordinates ############################### //

            $("#check-all-subordinates").on('click', function () {

                if ($('#check-all-subordinates').is(':checked') == true) {


                    $(".subordinate-checkbox").attr('checked', 'checked');
                } else {

                    $(".subordinate-checkbox").removeAttr('checked');
                }

                if ($('.subordinate-checkbox:checkbox:checked').length > 0) {
                    $('#delete-subordinate').removeAttr('disabled');
                } else {
                    $('#delete-subordinate').attr('disabled', 'disabled');
                }
            });
            $('.subordinate-checkbox').click(function () {
                if ($('.subordinate-checkbox:checkbox:checked').length > 0) {
                    $('#delete-subordinate').removeAttr('disabled');
                } else {
                    $('#delete-subordinate').attr('disabled', 'disabled');
                }
            });


            // #####################################################################


        }
    };
    Drupal.behaviors.showReportingManagerHints = {
        attach: function (context, settings) {
            $('#reporting-manager').autocomplete({
                source: drupalSettings.path.baseUrl + '/pim/reporting/getReportingManagers',
                select: function (event, ui) {
                    $('#reporting-manager-id').val(ui.item.desc);
                }
            });
        }
    };




})(jQuery, window, Drupal);