/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addLicense = {
        attach: function (context, settings) {
            $('#save-license').click(function (event) {
                var postData = {};
                event.preventDefault();
                var License = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'license_type': $('#license-type option:selected').val(),
                    'license_number': $('#license-number').val(),
                    'issued_date': $('#issued-date').val(),
                    'expiry_date': $('#expiry-date').val(),
                };


                postData['license'] = JSON.stringify(License);


                var postUrl = drupalSettings.path.baseUrl + 'pim/license/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('License Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editLicense = {
        attach: function (context, settings) {
            $('a#license').click(function () {

                var index = $(this).closest('tr').find('input').val();
                var id = index;
                console.log(index);
                var license_type = $('#license-type-' + index).text();
                var license_number = $('#license-number-' + index).text();
                var issued_date = $('#issued-date-' + index).text();
                var expiry_date = $('#expiry-date-' + index).text();

                $('#license-id').val(id);
               
                $('#license-type option').each(function(i,v){
                    if($.trim(license_type) == $.trim($(v).val())){
                        $(v).attr('selected', 'selected');
                    }
                });
                $('#license-number').val(license_number);
                $('#issued-date').val(issued_date);
                  $('#expiry-date').val(expiry_date);
               


                $('#license-panel').find('panel-title').html(Drupal.t('Edit License'));
                $('#license-panel').show();
                $('#add-license').hide();
                $('#delete-license').hide();

            });
        }
    };
    Drupal.behaviors.deleteLicense = {
        attach: function (context, settings) {
            $('#delete-license').on('click', function () {
                var checkboxes = new Array();

                $('.license-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

               
                var postUrl = drupalSettings.path.baseUrl + 'pim/license/delete';

                $.post(postUrl, postData, function (response) {
                   
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('License Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeLicenseCheckboxChecked = {
        attach: function (context, settings) {

           
            $("#check-all-licenses").on('click', function () {

                if ($('#check-all-licenses').is(':checked') == true) {


                    $(".license-checkbox").attr('checked', 'checked');
                } else {

                    $(".license-checkbox").removeAttr('checked');
                }

                if ($('.license-checkbox:checkbox:checked').length > 0) {
                    $('#delete-license').removeAttr('disabled');
                } else {
                    $('#delete-license').attr('disabled', 'disabled');
                }
            });
            $('.license-checkbox').click(function () {
                if ($('.license-checkbox:checkbox:checked').length > 0) {
                    $('#delete-license').removeAttr('disabled');
                } else {
                    $('#delete-license').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);