
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {


    'use strict';
    Drupal.behaviors.enableDisabledProp = {
        attach: function (context, settings) {
            // ######################################## PERSONAL DETAILS ################################################

            $('#personal-detail-attachment-panel').hide();
            $('#employee-edit-form #save-employee').hide();
            $("#edit-employee").click(function (event) {
                event.preventDefault();
                $(':disabled').prop("disabled", false);
                $(this).hide();
                $('#save-employee').show();

            });
            $('#add-attachement').click(function (event) {
                event.preventDefault();
                $('#personal-detail-attachment-panel').show();
                $(this).hide();
                $('#delete-attachement').hide();
            });
            $('#cancel-personal-detail-attachment').click(function (event) {
                event.preventDefault();
                $('#personal-detail-attachment-panel').hide();
                $('#delete-attachement').show();
                $('#add-attachement').show();
            });

            // ########################################################################################

            // ####################### Qualification details page     ############################33 //// #
            // ############################################################################################### //
            $('#work-experience-panel').hide();
            $('#education-panel').hide();
            $('#skill-panel').hide();
            $('#language-panel').hide();
            $('#license-panel').hide();
            $('#qualification-attachment-panel').hide();
            // ############### Hide Form on Cancel ##################3 //
            $('#cancel-work-experience').click(function (e) {
                e.preventDefault();
                $('#work-experience-panel').hide();
                $('#delete-work-experience').show();
                $('#add-work-experience').show();
            });
            $('#cancel-education').click(function (e) {
                e.preventDefault();
                $('#education-panel').hide();
                $('#delete-education').show();
                $('#add-education').show();
            });
            $('#cancel-skill').click(function (e) {
                e.preventDefault();
                $('#skill-panel').hide();
                $('#delete-skill').show();
                $('#add-skill').show();
            });
            $('#cancel-language').click(function (e) {
                e.preventDefault();
                $('#language-panel').hide();
                $('#delete-language').show();
                $('#add-language').show();
            });
            $('#cancel-license').click(function (e) {
                e.preventDefault();
                $('#license-panel').hide();
                $('#delete-license').show();
                $('#add-license').show();
            });
            $('#cancel-qualification-attachment').click(function (e) {
                e.preventDefault();
                $('#qualification-attachment-panel').hide();
                $('#delete-qualification-attachment').show();
                $('#add-qulification-attachement').show();
            });

            // #####################3 Show form when add button is clicked ####################### //

            $('#add-work-experience').click(function (e) {
                e.preventDefault();
                $('#work-experience-panel').show();
                $('#delete-work-experience').hide();
                $('#add-work-experience').hide();
            });
            $('#add-education').click(function (e) {
                e.preventDefault();
                $('#education-panel').show();
                $('#delete-education').hide();
                $('#add-education').hide();
            });
            $('#add-skill').click(function (e) {
                e.preventDefault();
                $('#skill-panel').show();
                $('#delete-skill').hide();
                $(this).hide();
            });
            $('#add-language').click(function (e) {
                e.preventDefault();
                $('#language-panel').show();
                $('#delete-language').hide();
                $(this).hide();
            });
            $('#add-license').click(function (e) {
                e.preventDefault();
                $('#license-panel').show();
                $('#delete-license').hide();
                $(this).hide();
            });
            $('#add-qulification-attachement').click(function (e) {
                e.preventDefault();
                $('#qualification-attachment-panel').show();
                $('#delete-qualification-attachment').hide();
                $(this).hide();
            });
            //################################################  REPORTING DETAILS ################################################################
            $('#reporting-panel').hide();
            $('#reporting-attachment-panel').hide();
            $('#upload-reporting-attachement').click(function (e) {
                e.preventDefault();

            });
            $('#cancel-reporting-attachment').click(function (e) {

                e.preventDefault();
                alert('asdf');
                $('#reporting-attachment-panel').hide();
                $('#add-reporting-attachement').show();
                $('#delete-reporting-attachement').show();
            });
            $('#add-reporting').click(function (e) {
                e.preventDefault();
                $('#reporting-panel').show();
                $(this).hide();
                $('#delete-reporting').hide();
                $('#reporting-manager-type').val('manager');
                 $('#reporting-panel .panel-heading h3').html('Add Manager');
            });
            $('#add-subordinate').click(function (e) {
                e.preventDefault();
                $('#reporting-panel').show();
                $('#reporting-panel .panel-heading h3').html('Add Subordinate');
                $(this).hide();
                $('#delete-reporting').hide();
                 $('#add-reporting').hide();
                 $('#delete-subordinate').hide();
                 $('#reporting-manager-type').val('subordinate');
            });
            $('#add-reporting-attachement').click(function (e) {
                e.preventDefault();
                $('#reporting-attachment-panel').show();
                $(this).hide();
                $('#delete-reporting-attachement').hide();
            });



            $('#cancel-reporting').click(function (e) {
                e.preventDefault();
                $('#reporting-panel').hide();
                $('#delete-reporting').show();
                $('#add-reporting').show();
                $('#add-subordinate').show();
                $('#delete-subordinate').show();
            });



            //################################################  REPORTING DETAILS ENDS ################################################################         
// ############################################ SALARY DETAIL STARTS ############################//
            $('#salary-panel').hide();
            $('td#account-number').hide();
            $('td#account-number').hide();
            $('td#account-number').hide();
            $('td#account-number').hide();
            $('#salary-attachment-panel').hide();
            // Reset form
             
            
            
            $('#add-salary').click(function (e) {
                e.preventDefault();
                $('#salary-panel').show();
                $(this).hide();
                $('#delete-salary').hide();
                
                //RESET FORM 
                $('#id').val();
                   
                    $('#pay-grade').val();
                    $('#salary-component').val();
                    $('#pay-frequency').val();
                     $('#currency option').each(function(i,v){
                   
                        $(v).removeAttr('selected');
                   
                });
                 $('#pay_frequency option').each(function(i, v){
                   
                        $(v).removeAttr('selected');
                   
                   
                });
                    $('#amount').val();
                    $('#comment').val();
                    $('#account-number').val();
                    $('#account-type option').each(function(i,v){
                     
                        $(v).removeAttr('selected');
                   
                });
                   
                    $('#routing-number').val();
                    $('#direct-deposit-amount').val();
                
                
                
            });
            $('#add-salary-attachement').click(function (e) {
                e.preventDefault();
                $('#salary-attachment-panel').show();
                $('#delete-salary-attachement').hide();
                $(this).hide();
            });
            $('#cancel-attachment').click(function (e) {
                e.preventDefault();
                $('#salary-attachment-panel').hide();
                $('#delete-salary-attachement').show();
                $('#add-salary-attachement').show();
            });
            $('#cancel-salary').click(function (e) {
                e.preventDefault();
                $('#salary-panel').hide();
                $('#delete-salary').show();
                $('#add-salary').show();
            });
// ################################ SALARY DETAILS ENDS ################################//

// ################################# IMMIGRATION DETAILS STARTS ##########################//

            $('#immigration-panel').hide();
            $('#immigration-attachment-panel').hide();
            $('#add-immigration').click(function (e) {
                e.preventDefault();
                $('#immigration-panel').show();
                $(this).hide();
                $('#delete-immigration').hide();

                // reset form

                $('#id').val('');
                //$('#employee-id').val(name);
                $('#document').removeAttr('checked');
                $('#number').val('');
                $('#issued-by').val('');
                $('#issued-date').val('');
                $('#expiry-date').val('');
                $('#comment').show();
                $('#eligible-status').val('');
                $('#eligible-review-date').show();

                $('#delete-immigration').hide();
                $(this).hide();




            });
            $('#add-immigration-attachement').click(function (e) {
                e.preventDefault();
                $('#immigration-attachment-panel').show();
                $(this).hide();
                $('#delete-immigration-attachement').hide();
            });
            $('#cancel-immigration').click(function (e) {
                e.preventDefault();
                $('#immigration-panel').hide();
                $('#delete-immigration').show();
                $('#add-immigration').show();
            });
            $('#cancel-immigration-attachment').click(function (e) {
                e.preventDefault();
                $('#immigration-attachment-panel').hide();
                $('#delete-immigration-attachement').show();
                $('#add-immigration-attachement').show();
            });



// ################################## IMMIGRATION ENDS ##################################33//

// ################################### DEPENDENTS STARTS #####################################//
            $('#dependent-panel').hide();
            $('#dependent-attachment-panel').hide();
            $('#add-dependent').click(function (e) {
                e.preventDefault();
                $('#dependent-panel').show();
                $('#delete-dependent').hide();
                $(this).hide();
            });
            $('#add-dependent-attachement').click(function (e) {
                e.preventDefault();
                $('#dependent-attachment-panel').show();
                $(this).hide();
                $('#delete-dependent-attachment').hide();
            });
            $('#cancel-dependent').click(function (e) {
                e.preventDefault();
                $('#dependent-panel').hide();
                $('#add-dependent').show();
                $('#delete-dependent').show();
            })
            $('#cancel-dependent-attachment').click(function (e) {
                e.preventDefault();
                $('#dependent-attachment-panel').hide();
                $('#delete-dependent-attachment').show();
                $('#add-dependent-attachement').show();
            })
// ################### Dependents Ends ########################//

// ####################### Job Starts #########################3//

            $('#job-attachment-panel').hide();
            $('#termination-modal').hide();
            $('#save-job').hide();
            $('#add-job-attachement').click(function (e) {
                $(this).hide();
                $('#delete-job-attachement').hide();
                e.preventDefault();
                $('#job-attachment-panel').show();
            });
            $('#add-job-attachement').click(function (e) {
                e.preventDefault();
                $('#job-attachment-panel').show();
            });
            $('#cancel-job-attachment').click(function (e) {
                e.preventDefault();
                $('#job-attachment-panel').hide();
                $('#delete-job-attachement').show();
                $('#add-job-attachement').show();
            });

            $("#edit-job").click(function (event) {
                event.preventDefault();
                $(':disabled').prop("disabled", false);
                $(this).hide();
                $('#save-job').show();

            });


// ####################### Job Ends ############################ //

            $('#emergency-contact-panel').hide();
            $('#emegency-contact-attachment-panel').hide();


            $('#add-emergency-contact').click(function (event) {
                event.preventDefault();
                $('#id').val('');
                //$('#employee-id').val(name);
                $('#name').val('');
                $('#relationship').val('');
                $('#home-phone').val('');
                $('#mobile').val('');
                $('#work-phone').val('');
                $('#emergency-contact-panel').show();
                $('#delete-emergency-contact').hide();
                $(this).hide();
            });

            $('#cancel-emergency-contact').click(function (event) {
                event.preventDefault();
                $('#emergency-contact-panel').hide();
                $('#delete-emergency-contact').show();
                $('#add-emergency-contact').show();
            });
            $('#cancel-emergency-contact-attachment').click(function (event) {
                event.preventDefault();
                $('#emegency-contact-attachment-panel').hide();
                $('#add-emergency-contact-attachement').show();
                $('#delete-emergency-contact-attachement').show();

            });

            $('#add-emergency-contact-attachement').click(function (event) {
                event.preventDefault();
                $('#emegency-contact-attachment-panel').show();
                $(this).hide();
                $('#delete-emergency-contact-attachement').hide();

            });




        }
    };
    /*
    Drupal.behaviors.terminateEmployee = {
        attach: function (context, settings) {
            $('#terminate-employee').click(function (e) {
                e.preventDefault();
                var employee_id = $('#employee-id').val();
                var postUrl = drupalSettings.path.baseUrl + 'pim/employee/terminate';
                var postData = {
                    'id': employee_id,
                    'employee_status': 1
                };
              // var data = JSON.stringify(postData);
                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Job Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewJobDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
            });
        }
    };
    */
    Drupal.behaviors.terminateEmployee = {
          attach: function (context, settings) {
            $('#terminate-employee').click(function (e) {
                e.preventDefault();
                     $('#termination-modal').modal('show');    
                });
          }
    };
    
    /*
     Drupal.behaviors.getEmployeeContactDetail = {
     attach: function(context, settings){
     $('a#contact-detail-link').click(function(e){
     e.preventDefault();
     var url = drupalSettings.path.baseUrl + 'pim/contactdetail/addForm';
     $.post(url, function(response){
     
     });
     });
     
     }
     };
     */

})(jQuery, window, Drupal);