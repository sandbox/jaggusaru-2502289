/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';
    Drupal.behaviors.enableDisabledContactDetailProp = {
        attach: function (context, settings) {
            $('#contact-detail-attachment-panel').hide();
            $('#employee-contact-form #save-employee-contact').hide();
            //$('#save-employee').hide();
            $("#edit-employee-contact").click(function (event) {
                event.preventDefault();
                $(':disabled').prop("disabled", false);
                $(this).hide();
                $('#save-employee-contact').show();

            });
            $('#add-attachement').click(function (event) {
                event.preventDefault();
                $('#contact-detail-attachment-panel').show();
                $(this).hide();
                $('#delete--attachement').hide();
            });
            $('#cancel-contact-detail-attachment').click(function (event) {
                event.preventDefault();
                $('#contact-detail-attachment-panel').hide();
                $('#delete-attachement').show();
                $('#add-attachement').show();
            });

        }
    };
    Drupal.behaviors.addEmployeeContact = {
        attach: function (context, settings) {



            $('#save-employee-contact').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Contact = {
                    'id': $('#contact-id').val(),
                    'employee_id':$('#employee-id').val(),
                    'address_street': $('#address-street').val(),
                    'address_street1': $('#address-street1').val(),
                    'city': $('#city').val(),
                    'state': $('#state').val(),
                    'zipcode': $('#zipcode').val(),
                    'country': $('#country').val(),
                    'home_phone': $('#home-phone').val(),
                    'mobile': $('#mobile').val(),
                    'work_phone': $('#work-phone').val(),
                    
                   'work_email'  : $('#work-email').val(),
                   'other_email' : $('#other-email').val(),
                };


                postData['contact'] = JSON.stringify(Contact);


                var postUrl = drupalSettings.path.baseUrl + 'pim/contactDetail/save';

                $.post(postUrl, postData, function (response) {
                    console.log('na');
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;
                    console.log('yes');
                    GeneralFunctions.displayNotification(Drupal.t('Contact Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewContactDetail/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };

})(jQuery, window, Drupal);