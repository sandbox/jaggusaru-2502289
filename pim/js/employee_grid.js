/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';


    Drupal.behaviors.showReportingManagerHintsOnEmployeeGrid = {
        attach: function (context, settings) {
            $('#employee-name').autocomplete({
                source: drupalSettings.path.baseUrl + '/pim/reporting/getReportingManagers',
                select: function (event, ui) {
                    $('#reporting-manager-id').val(ui.item.desc);
                }
            });
            $('#employee-id11').autocomplete({
                source: drupalSettings.path.baseUrl + '/pim/employee/getEmployeeIds',
                select: function (event, ui) {
                    $('#reporting-manager-id').val(ui.item.desc);
                    $('#employee-id').val(ui.item.desc);
                }
            });

            $('#manager-name').autocomplete({
                source: drupalSettings.path.baseUrl + '/pim/employee/getManagersHints',
                select: function (event, ui) {
                    $('#reporting-manager-id').val(ui.item.desc);
                }
            });
            $('#reset-employee').click(function () {
                $('#employee-name').val('');
                $('#employee-id').val('');
                $('#manager-name').val('');

            });

        }
    };
    Drupal.behaviors.searchEmployee = {
        attach: function (context, settings) {
            $('#search-employee').click(function (e) {
                var data = {};
                e.preventDefault();
                e.stopPropagation();
                var SearchParam = {
                    'first_name': $('#employee-name').val(),
                    'id': $('#employee-id').val(),
                    'manager': $('#supurvisor-name').val(),
                    'job_title': $('#job-titile').val(),
                    'sub_unit': $('#sub-unit').val(),
                    'employment_status': $('#employment-status').val(),
                    'employee_type': $('#include').val(),
                    'location': $('#location').val(),
                };
                    data['searchEmp'] = JSON.stringify(SearchParam);
                    var postUrl = drupalSettings.path.baseUrl + 'pim/employee/searchEmployees';
                     $.post(postUrl, data, function (response) {
                       
                        
                       console.log(response);
                          $('#employee-grid').html(response); 
                      
                
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;
                    console.log('yes');
                    GeneralFunctions.displayNotification(Drupal.t('Contact Details  saved'));
                    
 
                }, 'json');
                    
            });

            


        }

    };

})(jQuery, window, Drupal);