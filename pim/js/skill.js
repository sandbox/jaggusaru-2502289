/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addSkill = {
        attach: function (context, settings) {
            $('#save-skill').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Skill = {
                    'id': $('#id').val(),
                    'skill': $('#skill').val(),
                    'year_of_exp': $('#year-of-exp').val(),
                    'comment': $('#skill-comment').val(),
                    'employee_id': $('#employee-id').val(),
                };


                postData['skill'] = JSON.stringify(Skill);


                var postUrl = drupalSettings.path.baseUrl + 'pim/skill/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Skill Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editSkill = {
        attach: function (context, settings) {
            $('a#skill').click(function () {

                var index = $(this).closest('tr').find('input').val();
                var id = index;
                console.log(index);
                var skill = $('#skill-' + index).text();
                var year_of_exp = $('#year-of-exp-' + index).text();
                var comment = $('#skill-comment-' + index).text();
                

                $('#id').val(id);
               
                $('#skill').val(skill);
                $('#year-of-exp').val(year_of_exp);
                $('#skill-comment').val(comment);
               


                $('#skill-panel').find('panel-title').html(Drupal.t('Edit Skill'));
                $('#skill-panel').show();
                $('#add-skill').hide();
                $('#delete-skill').hide();

            });
        }
    };
    Drupal.behaviors.deleteSkill = {
        attach: function (context, settings) {
            $('#delete-skill').on('click', function () {
                var checkboxes = new Array();

                $('.skill-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

               
                var postUrl = drupalSettings.path.baseUrl + 'pim/skill/delete';

                $.post(postUrl, postData, function (response) {
                   
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Skill Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewQualificationDetails/' + response.employee;


                }, 'json');
            });

        }
    };
    Drupal.behaviors.makeSkillCheckboxChecked = {
        attach: function (context, settings) {

           
            $("#check-all-skills").on('click', function () {

                if ($('#check-all-skills').is(':checked') == true) {


                    $(".skill-checkbox").attr('checked', 'checked');
                } else {

                    $(".skill-checkbox").removeAttr('checked');
                }

                if ($('.skill-checkbox:checkbox:checked').length > 0) {
                    $('#delete-skill').removeAttr('disabled');
                } else {
                    $('#delete-skill').attr('disabled', 'disabled');
                }
            });
            $('.skill-checkbox').click(function () {
                if ($('.skill-checkbox:checkbox:checked').length > 0) {
                    $('#delete-skill').removeAttr('disabled');
                } else {
                    $('#delete-skill').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);