/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';
    Drupal.behaviors.enableDisabledProp = {
        attach: function (context, settings) {
            //$('#save-employee').hide();
            $("#edit-employee").click(function (event) {
                event.preventDefault();
                $(':disabled').prop("disabled", false);
                $(this).hide();
                $('#save-employee').show();

            });


        }
    };
    /*
Drupal.behaviors.getEmployeeContactDetail = {
    attach: function(context, settings){
        $('a#contact-detail-link').click(function(e){
             e.preventDefault();
            var url = drupalSettings.path.baseUrl + 'pim/contactdetail/addForm';
             $.post(url, function(response){
                 
             });
        });

    }
};
*/

})(jQuery, window, Drupal);