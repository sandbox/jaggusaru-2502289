/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';
    /*
     Drupal.behaviors.addFileUpload = {
     attach: function (context, settings) {
     
     $("#upload-personal-detail-attachement").click(function(){
     
     var formData = new FormData($('form')[0]);
     //formData.append("label", "WEBUPLOAD");
     //formData.append('file', $('#personal_detail_file')[0].files[0]);
     $.ajax({
     url: drupalSettings.path.baseUrl + 'pim/employee/uploadFile',
     type: 'GET',
     data: formData,
     //async: false,
     success: function (data) {
     alert(data)
     },
     cache: false,
     contentType: false,
     processData: false
     });
     
     return false;
     });
     */
    /* 
     
     $('#upload-personal-detail-attachement').click(function (event) {
     var postData = {};
     event.preventDefault();
     
     
     //var formData =  new FormData($('form')[0]);
     //  postData['employee'] = JSON.stringify(Employee);
     var formData = new FormData($('#upload-personal-detail-attachement')[0]);
     var data = $('#upload-personal-detail-attachement').serialize()
     var postUrl = drupalSettings.path.baseUrl + 'pim/employee/uploadFile';
     
     $.post(postUrl, formData, function (response) {
     if (!GeneralFunctions.handleAjaxExceptions(response))
     return;
     GeneralFunctions.displayNotification(Drupal.t('Employee  saved'));
     window.location= drupalSettings.path.baseUrl+ 'pim/employee/viewPersonalDetail/'+ response.employee;
     //GeneralFunctions.helper.resetForm();
     // console.log(response['employee']['id']['value']);
     
     }, 'json');
     // var formData =  new FormData($('form')[0]);
     });*/
    // }
    //};
    //
    ///*

    Drupal.behaviors.testupload = {
        attach: function (context, settings) {

            $("form#personalDetail").submit(function (evt) {
                evt.preventDefault();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: drupalSettings.path.baseUrl + 'pim/employee/uploadFile',
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    success: function (response) {
                        // $('#personal-detail-attachment-panel').hide();
                        console.log(response.success);
                        $('#personal-detail-attachment-panel').hide();
                        $('#add-attachement').show();
                        $('#delete-attachement').show();
                        $('#replace-attachment-grid').html(response.data);
                        // var html = '<div class="row"><div class="col-sm-12 alert alert-success" id="success-alert">' + '<button type="button" class="close" data-dismiss="alert">&times;</button>' + response.success + '</div></div>';
                        // $('#attachment-grid .panel-body div:first-child').after(html)
                        $('#success-alert').addClass('alert alert-success');
                        $('#success-alert').html(response.success);
                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                            $("#success-alert").alert('close');
                        });

                    }
                });

                return false;
            });



        }
    };
    Drupal.behaviors.contactDetailupload = {
        attach: function (context, settings) {

            $("form#contact-form").submit(function (evt) {
                evt.preventDefault();

                var formData = new FormData($(this)[0]);


                $.ajax({
                    url: drupalSettings.path.baseUrl + 'pim/employee/uploadFile',
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    success: function (response) {

                        $('#contact-detail-attachment-panel').hide();
                        $('#add-attachement').show();
                        $('#delete-attachement').show();
                       $('#delete-attachement').attr('disabled','disabled');
                        $('#success-alert').removeClass('hidden');
                        $('#success-alert').addClass('alert alert-success');
                        $('#success-alert p#display-message').html(response.success);
                       // $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        //    $("#success-alert").alert('close');
                        //});
 $('#replace-contact-attachment-grid').html(response.data);
                    },
                    error: function (response) {
                        if (response.status == 500) {
                            $('#success-alert').removeClass('hidden');

                         $('#success-alert').addClass('alert alert-danger');
                          $('#success-alert #display-message').html(Drupal.t('You do not have permission to upload.'));
                        }


                       // $("#success-alert").fadeTo(200000, 500).slideUp(500, function () {
                        //    $("#success-alert").alert('close');
                       // });
                    }
                });

                return false;
            });



        }
    };
    Drupal.behaviors.deleteFileAttachemtn = {
        attach: function (context, settings) {
            $('#delete-attachement').on('click', function () {
                var checkboxes = new Array();

                $('.file-checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();
                $.ajax({
                    url: drupalSettings.path.baseUrl + 'pim/employee/attachment/deleteFile',
                    type: 'POST',
                    data: postData,
                    async: false,
                    // cache: false,
                    // contentType: false,
                    //enctype: 'multipart/form-data',
                    // processData: false,
                    success: function (response) {
                        console.log(response);
                        consolwe.log('sadfd');
                        $('#personal-detail-attachment-panel').hide();
                        $('#add-attachement').show();
                        $('#delete-attachement').show();
                         $('#delete-attachement').attr('disabled','disabled');
                         $('#success-alert').addClass('alert alert-success');
                        $('#success-alert').html(response.success);
                        $('#replace-contact-attachment-grid').html(response.data);

                        
                      //  $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        //    $("#success-alert").alert('close');
                       // });

                    },
                     error: function (response) {
                        if (response.status == 500) {
                            $('#success-alert').removeClass('hidden');

                         $('#success-alert').addClass('alert alert-danger');
                          $('#success-alert #error-text').html(Drupal.t('You do not have permission to delete.'));
                        }


                      $("#success-alert").fadeTo(200000, 500).slideUp(500, function () {
                            $("#success-alert").alert('close');
                        });
                    }
                });

            });

        }
    };
    Drupal.behaviors.makeAtachmentCheckboxChecked = {
        attach: function (context, settings) {


            $("#check-all-file").on('click', function () {

                if ($('#check-all-file').is(':checked') == true) {
console.log('yes');

                    $(".file-checkbox").attr('checked', 'checked');
                } else {
console.log('no');
                    $(".file-checkbox").removeAttr('checked');
                }

                if ($('.file-checkbox:checkbox:checked').length > 0) {
                    $('#delete-attachement').removeAttr('disabled');
                } else {
                    $('#delete-attachement').attr('disabled', 'disabled');
                }
            });
            $('.file-checkbox').on('click',function () {
                console.log($('.file-checkbox:checkbox:checked').length);

                if ($('.file-checkbox:checkbox:checked').length > 0) {
                    $('#delete-attachement').removeAttr('disabled');
                } else {
                    $('#delete-attachement').attr('disabled', 'disabled');
                }
            });


        }
    };

})(jQuery, window, Drupal);


