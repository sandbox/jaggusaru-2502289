/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addEmployee = {
        attach: function (context, settings) {

             

            $('#save-employee').click(function (event) {
                 var postData = {};
                event.preventDefault();

                var name = $('#username').val();

                var pass = $('#password').val();
                if(name){
                    if(pass){
                        Account = {
                        'name': $('#username').val(),
                        'pass': $('#password').val(),
                        //'is_enabled': $('#is-enabled').val(),
                       
                    };
                     postData['account'] = JSON.stringify(Account);
                    }
                    }
                    

                var Employee = {
                    'id' : $('#employee-id').val(),
                    'first_name': $('#first-name').val(),
                    'middle_name': $('#middle-name').val(),
                    'last_name': $('#last-name').val(),
                    'driver_license' : $('#driver-license-number').val(),
                    'license_expiry_date': $('#license-expiry-date').val(),
                    'birth_date' : $('#birth-date input').val(),
                    'gender' : $('#gender').val(),
                    'marital_status': $('#marital-status').val(),
                    'nationality' : $('#nationality').val(),
                    
                };

              
                postData['employee'] = JSON.stringify(Employee);
                

                var postUrl = drupalSettings.path.baseUrl + 'pim/employee/save';

                $.post(postUrl, postData, function (response) {
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;
                    GeneralFunctions.displayNotification(Drupal.t('Employee  saved'));
                  window.location= drupalSettings.path.baseUrl+ 'pim/employee/viewPersonalDetail/'+ response.employee;
                    //GeneralFunctions.helper.resetForm();
                   // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    /**
     * Event: Admin, Provider, Secretary Username "Focusout" 
     * 
     * When the user leaves the username input field we will need to check if the username 
     * is not taken by another record in the system. Usernames must be unique.
     */
    Drupal.behaviors.ajaxValidateUserName = {
        attach: function (context, settings) {

            jQuery('#username').focusout(function () {
                var jQueryinput = jQuery(this);
                console.log(jQueryinput);
                if (jQueryinput.prop('readonly') == true || jQueryinput.val() == '') {
                    return;
                }

                var postUrl = drupalSettings.path.baseUrl + 'pim/user/validateUserName';
                var postData = {
                    'username': jQueryinput.val(),
                    //'user_id': jQueryinput.parents().eq(2).find('.record-id').val()
                };

                jQuery.post(postUrl, postData, function (response) {
                    ///////////////////////////////////////////////////////
                    console.log('Validate Username Response:', response);
                    ///////////////////////////////////////////////////////
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;
                    if (response == false) {

                        jQueryinput.css('border', '2px solid red');
                        jQueryinput.attr('already-exists', 'true');
                        jQueryinput.parents().eq(3).find('.form-message').text(Drupal.t('Username already exists'));
                        jQueryinput.parents().eq(3).find('.form-message').show();
                    } else {

                        jQueryinput.css('border', '');
                        jQueryinput.attr('already-exists', 'false');
                        if (jQueryinput.parents().eq(3).find('.form-message').text() == Drupal.t('Username already exists')) {
                            jQueryinput.parents().eq(3).find('.form-message').hide();
                        }
                    }
                }, 'json');
            });


        }
    };
    Drupal.behaviors.checkPasswordMatch = {
        attach: function (context, settings) {

            $('#confirm-password').focusout(function () {
                console.log('asdfsd');
                var password = $("#password").val();
                var confirmPassword = $("#confirm-password").val();

                if (password != confirmPassword) {
                    $("#CheckPasswordMatch").html("Passwords do not match!");
                    $("#CheckPasswordMatch").css('color', 'red');
                } else {
                    $("#CheckPasswordMatch").html("Passwords match.");
                    $("#CheckPasswordMatch").css('color', 'green');
                }
            });


        }
    };
Drupal.behaviors.dateTimePicker = {
        attach: function(context, settings){
             $('#license-expiry-date1').click(function(event){
                 event.preventDefault();
                 $(this).datepicker();
             });
        }
    };
 Drupal.behaviors.display = {
     attach : function(context, settings){
         $('#first-name').val();
     }
 };
})(jQuery, window, Drupal);


