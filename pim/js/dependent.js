/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addDependent = {
        attach: function (context, settings) {
            $('#save-dependent').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Dependent = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'name': $('#name').val(),
                    'relationship': $('#relationship').val(),
                    'birth_date': $('#birth-date').val(),
                    
                };


                postData['dependent'] = JSON.stringify(Dependent);


                var postUrl = drupalSettings.path.baseUrl + 'pim/dependent/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Dependent Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewDependentDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    Drupal.behaviors.editDependent = {
        attach: function (context, settings) {
            $('a').click(function () {
                //console.log();
                var index = $(this).closest('tr').find('input').val();
                var id = index;
              //console.log(id);
                var name = $('#name-' + index).find('a').text();
                var relationship = $('#relationship-' + index).text();
                var birth_date = $('#birth-date-' + index).text();
               
                $('#id').val(id);
                //$('#employee-id').val(name);
                $('#name').val(name);
                $('#relationship').val(relationship);
                $('#birth-date').val(birth_date);
                
                $('#dependent-panel').find('panel-title').html('Edit Emergency Contact');
                $('#dependent-panel').show();
                $('#add-dependent').hide();
                $('#delete-dependent').hide();

            });
        }
    };
    Drupal.behaviors.deleteDependent = {
        attach: function (context, settings) {
            $('#delete-dependent').on('click', function(){
                 var checkboxes = new Array();
            
            $('.checkbox:checked').each(function(){
               checkboxes.push($(this).val());
                //console.log('in');
            });
            var postData = {};
             postData['ids'] = checkboxes;
            postData['employee_id'] = $('#employee-id').val();
                
           // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/dependent/delete';

                $.post(postUrl, postData, function (response) {
                   // console.log(response);return false;
                           // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Dependent Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewDependentDetails/' + response.employee;
                   

                }, 'json');
            });
           
        }
    };
    Drupal.behaviors.makeCheckboxChecked = {
        attach: function (context, settings) {
            
          /*

        $("#check_all_emergency_contact").click(function(){
            if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
                $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
            } else {
                $('#delete-emergency_contact-checkbox').removeAttr('checked');
            }
            */
           $("#check-all").on('click',function(){
                console.log($("#check-all:checked").attr('value'));
              console.log($('#check-all').is(':checked'));
               if($('#check-all').is(':checked') == true){
                   
                
                $(".checkbox").attr('checked', 'checked');
            } else {
                console.log('asdfsdaasdfasdfsadf');
                $(".checkbox").removeAttr('checked');
            }
       /*

            $('#check-all').bind('click',function () {

                if ($(this).is(':checked')) {
                    $('.checkbox').attr('checked', 'checked');
                    //console.log($('.checkbox:checkbox:checked').length);
                } else {
                    $('.checkbox').removeAttr('checked');
                }*/
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-dependent').removeAttr('disabled');
            } else {
                $('#delete-dependent').attr('disabled', 'disabled');
            }
            });
            $('.checkbox').click(function(){
                if($('.checkbox:checkbox:checked').length > 0) {
                 $('#delete-dependent').removeAttr('disabled');
            } else {
                $('#delete-dependent').attr('disabled', 'disabled');
            }
            });
            
             
        }
    };
})(jQuery, window, Drupal);