/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($, window, Drupal) {

    'use strict';

    Drupal.behaviors.addJob = {
        attach: function (context, settings) {
            $('#save-job').click(function (event) {
                var postData = {};
                event.preventDefault();
                var Job = {
                    'id': $('#id').val(),
                    'employee_id': $('#employee-id').val(),
                    'job_title': $('#job-title option:selected').val(),
                    'job_specification': $('#job-specification').val(),
                    'job_category': $('#job-category option:selected').val(),
                    'employment_status': $('#employment-status option:selected').val(),
                    'sub_unit': $('#sub-unit option:selected').val(),
                    'joined_date': $('#joined-date').val(),
                    'location': $('#location option:selected').val(),
                    'contract_start_date': $('#contract-start-date').val(),
                    'contract_end_date': $('#contract-end-date').val()
                };


                postData['job'] = JSON.stringify(Job);


                var postUrl = drupalSettings.path.baseUrl + 'pim/job/save';

                $.post(postUrl, postData, function (response) {

                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Job Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewJobDetails/' + response.employee;
                    //GeneralFunctions.helper.resetForm();
                    // console.log(response['employee']['id']['value']);

                }, 'json');
                // var formData =  new FormData($('form')[0]);
            });
        }
    };
    /*
    Drupal.behaviors.editJob = {
        attach: function (context, settings) {
            $('a').click(function () {

                var index = $(this).closest('tr').find('input').val();
                var id = index;
                var birth_date = $('#birth-date-' + index).text();
                var job_titile = $('#job-title-' + index).text();
                var job_specification = $('#job-specification-' + index).text();
                var job_category = $('#job-category-' + index).text();
                var employement_status = $('#employment-status-' + index).text();
                var sub_unit = $('#sub-unit-' + index).text();
                var joined_date = $('#joined-date-' + index).text();
                var location = $('#location-' + index).text();
                var contract_start_date = $('#contract-start-date-' + index).text();
                var contract_end_date = $('#contract-end-date-' + index).text();




                $('#id').val(id);
                //$('#employee-id').val(name);
                
                 $('#job-title-').val(job_titile);
                $('#job-specification').val(job_specification );
                $('#job-category').val(job_category);
                $('#employment-status').val(employement_status);
                $('#sub-unit').val(sub_unit);
                $('#joined-date').val(joined_date);
                $('#location-').val(location);
                $('#contract-start-date').val(contract_start_date);
                $('#contract-end-date').val(contract_end_date);



                $('#job-panel').find('panel-title').html('Edit Job');
                $('#dependent-panel').show();
                $('#add-dependent').hide();
                $('#delete-dependent').hide();

            });
        }
    };
    Drupal.behaviors.deleteDependent = {
        attach: function (context, settings) {
            $('#delete-dependent').on('click', function () {
                var checkboxes = new Array();

                $('.checkbox:checked').each(function () {
                    checkboxes.push($(this).val());
                    //console.log('in');
                });
                var postData = {};
                postData['ids'] = checkboxes;
                postData['employee_id'] = $('#employee-id').val();

                // console.log(checkboxes);
                var postUrl = drupalSettings.path.baseUrl + 'pim/dependent/delete';

                $.post(postUrl, postData, function (response) {
                    // console.log(response);return false;
                    // $('div.error p').html(response.status);return false;
                    if (!GeneralFunctions.handleAjaxExceptions(response))
                        return;

                    GeneralFunctions.displayNotification(Drupal.t('Dependent Details  saved'));
                    window.location = drupalSettings.path.baseUrl + 'pim/employee/viewDependentDetails/' + response.employee;


                }, 'json');
            });

        }
    };*/
    Drupal.behaviors.makeCheckboxChecked = {
        attach: function (context, settings) {

            /*
             
             $("#check_all_emergency_contact").click(function(){
             if($("#check_all_emergency_contact:checked").attr('value') == 'on') {
             $('#delete-emergency_contact-checkbox').attr('checked', 'checked');
             } else {
             $('#delete-emergency_contact-checkbox').removeAttr('checked');
             }
             */
            $("#check-all").on('click', function () {
                console.log($("#check-all:checked").attr('value'));
                console.log($('#check-all').is(':checked'));
                if ($('#check-all').is(':checked') == true) {


                    $(".checkbox").attr('checked', 'checked');
                } else {
                    console.log('asdfsdaasdfasdfsadf');
                    $(".checkbox").removeAttr('checked');
                }
                /*
                 
                 $('#check-all').bind('click',function () {
                 
                 if ($(this).is(':checked')) {
                 $('.checkbox').attr('checked', 'checked');
                 //console.log($('.checkbox:checkbox:checked').length);
                 } else {
                 $('.checkbox').removeAttr('checked');
                 }*/
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-dependent').removeAttr('disabled');
                } else {
                    $('#delete-dependent').attr('disabled', 'disabled');
                }
            });
            $('.checkbox').click(function () {
                if ($('.checkbox:checkbox:checked').length > 0) {
                    $('#delete-dependent').removeAttr('disabled');
                } else {
                    $('#delete-dependent').attr('disabled', 'disabled');
                }
            });


        }
    };
})(jQuery, window, Drupal);