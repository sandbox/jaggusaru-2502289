<?php

namespace Drupal\pim\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the pim module.
 */
class ContactDetailControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "pim ContactDetailController's controller functionality",
      'description' => 'Test Unit for module pim and controller ContactDetailController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests pim functionality.
   */
  public function testContactDetailController() {
    // Check that the basic functions of module pim.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
