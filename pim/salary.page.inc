<?php

/**
 * @file
 * Contains salary.page.inc.
 *
 * Page callback for Salary entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Salary templates.
 *
 * Default template: salary.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_salary(array &$variables) {
  // Fetch Salary Entity Object.
  $salary = $variables['elements']['#salary'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
