<?php

namespace Drupal\job_title\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Job title entities.
 */
class JobTitleViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['job_title']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Job title'),
      'help' => $this->t('The Job title ID.'),
    );

    return $data;
  }

}
