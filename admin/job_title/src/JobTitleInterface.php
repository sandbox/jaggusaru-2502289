<?php

namespace Drupal\job_title;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job title entities.
 *
 * @ingroup job_title
 */
interface JobTitleInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Job title name.
   *
   * @return string
   *   Name of the Job title.
   */
  public function getName();

  /**
   * Sets the Job title name.
   *
   * @param string $name
   *   The Job title name.
   *
   * @return \Drupal\job_title\JobTitleInterface
   *   The called Job title entity.
   */
  public function setName($name);

  /**
   * Gets the Job title creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job title.
   */
  public function getCreatedTime();

  /**
   * Sets the Job title creation timestamp.
   *
   * @param int $timestamp
   *   The Job title creation timestamp.
   *
   * @return \Drupal\job_title\JobTitleInterface
   *   The called Job title entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Job title published status indicator.
   *
   * Unpublished Job title are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Job title is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Job title.
   *
   * @param bool $published
   *   TRUE to set this Job title to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\job_title\JobTitleInterface
   *   The called Job title entity.
   */
  public function setPublished($published);

}
