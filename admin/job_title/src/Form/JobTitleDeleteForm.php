<?php

namespace Drupal\job_title\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job title entities.
 *
 * @ingroup job_title
 */
class JobTitleDeleteForm extends ContentEntityDeleteForm {

}
