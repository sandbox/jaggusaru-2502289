<?php

namespace Drupal\job_title;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Job title entity.
 *
 * @see \Drupal\job_title\Entity\JobTitle.
 */
class JobTitleAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\job_title\JobTitleInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished job title entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job title entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit job title entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job title entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job title entities');
  }

}
