<?php
namespace Drupal\job_title;

use Drupal\job_title\Entity\JobTitle;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JobTitleManager {

    public static function getJobTitles() {
        $query = \Drupal::entityQuery('job_title');


        $nids = $query->execute();
        return JobTitle::loadMultiple($nids);
    }

}
