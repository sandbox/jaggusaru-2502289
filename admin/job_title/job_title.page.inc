<?php

/**
 * @file
 * Contains job_title.page.inc.
 *
 * Page callback for Job title entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Job title templates.
 *
 * Default template: job_title.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_job_title(array &$variables) {
  // Fetch JobTitle Entity Object.
  $job_title = $variables['elements']['#job_title'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
