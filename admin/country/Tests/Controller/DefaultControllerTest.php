<?php

namespace Drupal\country\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the country module.
 */
class DefaultControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "country DefaultController's controller functionality",
      'description' => 'Test Unit for module country and controller DefaultController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests country functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module country.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
