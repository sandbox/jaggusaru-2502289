<?php

namespace Drupal\country\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Test entity entities.
 */
class TestEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['test_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Test entity'),
      'help' => $this->t('The Test entity ID.'),
    );

    return $data;
  }

}
