<?php

namespace Drupal\country;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Test entity entities.
 *
 * @ingroup country
 */
interface TestEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Test entity name.
   *
   * @return string
   *   Name of the Test entity.
   */
  public function getName();

  /**
   * Sets the Test entity name.
   *
   * @param string $name
   *   The Test entity name.
   *
   * @return \Drupal\country\TestEntityInterface
   *   The called Test entity entity.
   */
  public function setName($name);

  /**
   * Gets the Test entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Test entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Test entity creation timestamp.
   *
   * @param int $timestamp
   *   The Test entity creation timestamp.
   *
   * @return \Drupal\country\TestEntityInterface
   *   The called Test entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Test entity published status indicator.
   *
   * Unpublished Test entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Test entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Test entity.
   *
   * @param bool $published
   *   TRUE to set this Test entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\country\TestEntityInterface
   *   The called Test entity entity.
   */
  public function setPublished($published);

}
