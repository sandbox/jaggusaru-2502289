<?php

namespace Drupal\country\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Test entity entities.
 *
 * @ingroup country
 */
class TestEntityDeleteForm extends ContentEntityDeleteForm {

}
