<?php
namespace Drupal\country;
use Drupal\country\Entity\Country;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CountryManager{
    public static function getCountries(){
        $query = \Drupal::entityQuery('country');
           
            
        $nids = $query->execute();
        return Country::loadMultiple($nids);
    }
}