<?php

/**
 * @file
 * Contains currency.page.inc.
 *
 * Page callback for Currency entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Currency templates.
 *
 * Default template: currency.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_currency(array &$variables) {
  // Fetch Currency Entity Object.
  $currency = $variables['elements']['#currency'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
