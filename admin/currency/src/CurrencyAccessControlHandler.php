<?php

namespace Drupal\currency;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Currency entity.
 *
 * @see \Drupal\currency\Entity\Currency.
 */
class CurrencyAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\currency\CurrencyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished currency entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published currency entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit currency entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete currency entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add currency entities');
  }

}
