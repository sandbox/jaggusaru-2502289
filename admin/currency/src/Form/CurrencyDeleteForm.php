<?php

namespace Drupal\currency\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Currency entities.
 *
 * @ingroup currency
 */
class CurrencyDeleteForm extends ContentEntityDeleteForm {

}
