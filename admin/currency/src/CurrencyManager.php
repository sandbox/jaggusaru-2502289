<?php
namespace Drupal\currency;
use Drupal\currency\Entity\Currency;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CurrencyManager{
    public static function getCurrencies(){
        $query = \Drupal::entityQuery('currency');
           
            
        $nids = $query->execute();
        return Currency::loadMultiple($nids);
    }
}