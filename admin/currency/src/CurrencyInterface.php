<?php

namespace Drupal\currency;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Currency entities.
 *
 * @ingroup currency
 */
interface CurrencyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Currency name.
   *
   * @return string
   *   Name of the Currency.
   */
  public function getName();

  /**
   * Sets the Currency name.
   *
   * @param string $name
   *   The Currency name.
   *
   * @return \Drupal\currency\CurrencyInterface
   *   The called Currency entity.
   */
  public function setName($name);

  /**
   * Gets the Currency creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Currency.
   */
  public function getCreatedTime();

  /**
   * Sets the Currency creation timestamp.
   *
   * @param int $timestamp
   *   The Currency creation timestamp.
   *
   * @return \Drupal\currency\CurrencyInterface
   *   The called Currency entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Currency published status indicator.
   *
   * Unpublished Currency are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Currency is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Currency.
   *
   * @param bool $published
   *   TRUE to set this Currency to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\currency\CurrencyInterface
   *   The called Currency entity.
   */
  public function setPublished($published);

}
