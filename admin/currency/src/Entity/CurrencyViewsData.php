<?php

namespace Drupal\currency\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Currency entities.
 */
class CurrencyViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['currency']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Currency'),
      'help' => $this->t('The Currency ID.'),
    );

    return $data;
  }

}
