<?php

namespace Drupal\pay_frequency;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Pay frequency entity.
 *
 * @see \Drupal\pay_frequency\Entity\PayFrequency.
 */
class PayFrequencyAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\pay_frequency\PayFrequencyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished pay frequency entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published pay frequency entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit pay frequency entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete pay frequency entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add pay frequency entities');
  }

}
