<?php

namespace Drupal\pay_frequency\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Pay frequency entities.
 *
 * @ingroup pay_frequency
 */
class PayFrequencyDeleteForm extends ContentEntityDeleteForm {

}
