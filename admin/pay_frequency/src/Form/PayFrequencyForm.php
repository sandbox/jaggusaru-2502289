<?php

namespace Drupal\pay_frequency\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Pay frequency edit forms.
 *
 * @ingroup pay_frequency
 */
class PayFrequencyForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\pay_frequency\Entity\PayFrequency */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Pay frequency.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Pay frequency.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.pay_frequency.canonical', ['pay_frequency' => $entity->id()]);
  }

}
