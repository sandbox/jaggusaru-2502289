<?php
namespace Drupal\pay_frequency;
use Drupal\pay_frequency\Entity\PayFrequency;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PayFrequencyManager{
    public static function getPayFrequencies(){
        $query = \Drupal::entityQuery('pay_frequency');
           
            
        $nids = $query->execute();
        return PayFrequency::loadMultiple($nids);
    }
}