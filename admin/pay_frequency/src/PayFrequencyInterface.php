<?php

namespace Drupal\pay_frequency;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Pay frequency entities.
 *
 * @ingroup pay_frequency
 */
interface PayFrequencyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Pay frequency name.
   *
   * @return string
   *   Name of the Pay frequency.
   */
  public function getName();

  /**
   * Sets the Pay frequency name.
   *
   * @param string $name
   *   The Pay frequency name.
   *
   * @return \Drupal\pay_frequency\PayFrequencyInterface
   *   The called Pay frequency entity.
   */
  public function setName($name);

  /**
   * Gets the Pay frequency creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Pay frequency.
   */
  public function getCreatedTime();

  /**
   * Sets the Pay frequency creation timestamp.
   *
   * @param int $timestamp
   *   The Pay frequency creation timestamp.
   *
   * @return \Drupal\pay_frequency\PayFrequencyInterface
   *   The called Pay frequency entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Pay frequency published status indicator.
   *
   * Unpublished Pay frequency are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Pay frequency is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Pay frequency.
   *
   * @param bool $published
   *   TRUE to set this Pay frequency to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\pay_frequency\PayFrequencyInterface
   *   The called Pay frequency entity.
   */
  public function setPublished($published);

}
