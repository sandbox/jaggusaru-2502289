<?php

namespace Drupal\pay_frequency\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Pay frequency entities.
 */
class PayFrequencyViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pay_frequency']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Pay frequency'),
      'help' => $this->t('The Pay frequency ID.'),
    );

    return $data;
  }

}
