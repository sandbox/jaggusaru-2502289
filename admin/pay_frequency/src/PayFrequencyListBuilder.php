<?php

namespace Drupal\pay_frequency;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Pay frequency entities.
 *
 * @ingroup pay_frequency
 */
class PayFrequencyListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Pay frequency ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\pay_frequency\Entity\PayFrequency */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.pay_frequency.edit_form', array(
          'pay_frequency' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
