<?php

/**
 * @file
 * Contains pay_frequency.page.inc.
 *
 * Page callback for Pay frequency entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Pay frequency templates.
 *
 * Default template: pay_frequency.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_pay_frequency(array &$variables) {
  // Fetch PayFrequency Entity Object.
  $pay_frequency = $variables['elements']['#pay_frequency'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
