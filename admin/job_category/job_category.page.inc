<?php

/**
 * @file
 * Contains job_category.page.inc.
 *
 * Page callback for Job category entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Job category templates.
 *
 * Default template: job_category.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_job_category(array &$variables) {
  // Fetch JobCategory Entity Object.
  $job_category = $variables['elements']['#job_category'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
