<?php

namespace Drupal\job_category;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Job category entity.
 *
 * @see \Drupal\job_category\Entity\JobCategory.
 */
class JobCategoryAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\job_category\JobCategoryInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished job category entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job category entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit job category entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job category entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job category entities');
  }

}
