<?php
namespace Drupal\job_category;

use Drupal\job_category\Entity\JobCategory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JobCategoryManager {

    public static function getJobCategories() {
        $query = \Drupal::entityQuery('job_category');


        $nids = $query->execute();
        return JobCategory::loadMultiple($nids);
    }

}
