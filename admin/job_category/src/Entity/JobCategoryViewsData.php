<?php

namespace Drupal\job_category\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Job category entities.
 */
class JobCategoryViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['job_category']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Job category'),
      'help' => $this->t('The Job category ID.'),
    );

    return $data;
  }

}
