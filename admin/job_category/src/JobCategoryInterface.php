<?php

namespace Drupal\job_category;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job category entities.
 *
 * @ingroup job_category
 */
interface JobCategoryInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Job category name.
   *
   * @return string
   *   Name of the Job category.
   */
  public function getName();

  /**
   * Sets the Job category name.
   *
   * @param string $name
   *   The Job category name.
   *
   * @return \Drupal\job_category\JobCategoryInterface
   *   The called Job category entity.
   */
  public function setName($name);

  /**
   * Gets the Job category creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job category.
   */
  public function getCreatedTime();

  /**
   * Sets the Job category creation timestamp.
   *
   * @param int $timestamp
   *   The Job category creation timestamp.
   *
   * @return \Drupal\job_category\JobCategoryInterface
   *   The called Job category entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Job category published status indicator.
   *
   * Unpublished Job category are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Job category is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Job category.
   *
   * @param bool $published
   *   TRUE to set this Job category to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\job_category\JobCategoryInterface
   *   The called Job category entity.
   */
  public function setPublished($published);

}
