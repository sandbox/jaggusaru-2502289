<?php

namespace Drupal\job_category\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job category entities.
 *
 * @ingroup job_category
 */
class JobCategoryDeleteForm extends ContentEntityDeleteForm {

}
