<?php

/**
 * @file
 * Contains sub_unit.page.inc.
 *
 * Page callback for Sub unit entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Sub unit templates.
 *
 * Default template: sub_unit.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_sub_unit(array &$variables) {
  // Fetch SubUnit Entity Object.
  $sub_unit = $variables['elements']['#sub_unit'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
