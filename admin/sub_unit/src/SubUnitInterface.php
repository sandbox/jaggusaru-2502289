<?php

namespace Drupal\sub_unit;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Sub unit entities.
 *
 * @ingroup sub_unit
 */
interface SubUnitInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Sub unit name.
   *
   * @return string
   *   Name of the Sub unit.
   */
  public function getName();

  /**
   * Sets the Sub unit name.
   *
   * @param string $name
   *   The Sub unit name.
   *
   * @return \Drupal\sub_unit\SubUnitInterface
   *   The called Sub unit entity.
   */
  public function setName($name);

  /**
   * Gets the Sub unit creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Sub unit.
   */
  public function getCreatedTime();

  /**
   * Sets the Sub unit creation timestamp.
   *
   * @param int $timestamp
   *   The Sub unit creation timestamp.
   *
   * @return \Drupal\sub_unit\SubUnitInterface
   *   The called Sub unit entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Sub unit published status indicator.
   *
   * Unpublished Sub unit are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Sub unit is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Sub unit.
   *
   * @param bool $published
   *   TRUE to set this Sub unit to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\sub_unit\SubUnitInterface
   *   The called Sub unit entity.
   */
  public function setPublished($published);

}
