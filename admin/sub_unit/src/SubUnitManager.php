<?php
namespace Drupal\sub_unit;

use Drupal\sub_unit\Entity\SubUnit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SubUnitManager {

    public static function getSubUnits() {
        $query = \Drupal::entityQuery('sub_unit');


        $nids = $query->execute();
        return SubUnit::loadMultiple($nids);
    }

}
