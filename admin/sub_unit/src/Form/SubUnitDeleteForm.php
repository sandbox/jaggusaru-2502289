<?php

namespace Drupal\sub_unit\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sub unit entities.
 *
 * @ingroup sub_unit
 */
class SubUnitDeleteForm extends ContentEntityDeleteForm {

}
