<?php

namespace Drupal\sub_unit;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sub unit entity.
 *
 * @see \Drupal\sub_unit\Entity\SubUnit.
 */
class SubUnitAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\sub_unit\SubUnitInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sub unit entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published sub unit entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit sub unit entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete sub unit entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add sub unit entities');
  }

}
