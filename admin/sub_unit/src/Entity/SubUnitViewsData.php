<?php

namespace Drupal\sub_unit\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Sub unit entities.
 */
class SubUnitViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['sub_unit']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Sub unit'),
      'help' => $this->t('The Sub unit ID.'),
    );

    return $data;
  }

}
