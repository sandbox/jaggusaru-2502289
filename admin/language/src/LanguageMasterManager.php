<?php
namespace Drupal\language;
use Drupal\language\Entity\Language;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LanguageMasterManager{
    public static function getLanguageMasters(){
        $query = \Drupal::entityQuery('language_master');
           
            
        $nids = $query->execute();
        return Language::loadMultiple($nids);
    }
}