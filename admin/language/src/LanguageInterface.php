<?php

namespace Drupal\language;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Language Master entities.
 *
 * @ingroup language
 */
interface LanguageInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Language Master name.
   *
   * @return string
   *   Name of the Language Master.
   */
  public function getName();

  /**
   * Sets the Language Master name.
   *
   * @param string $name
   *   The Language Master name.
   *
   * @return \Drupal\language\LanguageInterface
   *   The called Language Master entity.
   */
  public function setName($name);

  /**
   * Gets the Language Master creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Language Master.
   */
  public function getCreatedTime();

  /**
   * Sets the Language Master creation timestamp.
   *
   * @param int $timestamp
   *   The Language Master creation timestamp.
   *
   * @return \Drupal\language\LanguageInterface
   *   The called Language Master entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Language Master published status indicator.
   *
   * Unpublished Language Master are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Language Master is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Language Master.
   *
   * @param bool $published
   *   TRUE to set this Language Master to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\language\LanguageInterface
   *   The called Language Master entity.
   */
  public function setPublished($published);

}
