<?php

namespace Drupal\language;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Language Master entity.
 *
 * @see \Drupal\language\Entity\Language.
 */
class LanguageAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\language\LanguageInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished language master entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published language master entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit language master entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete language master entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add language master entities');
  }

}
