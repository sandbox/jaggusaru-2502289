<?php

namespace Drupal\language\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Language Master entities.
 */
class LanguageViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['language_master']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Language Master'),
      'help' => $this->t('The Language Master ID.'),
    );

    return $data;
  }

}
