<?php

namespace Drupal\language\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Language Master entities.
 *
 * @ingroup language
 */
class LanguageDeleteForm extends ContentEntityDeleteForm {

}
