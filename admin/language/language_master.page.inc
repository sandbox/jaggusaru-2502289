<?php

/**
 * @file
 * Contains language_master.page.inc.
 *
 * Page callback for Language Master entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Language Master templates.
 *
 * Default template: language_master.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_language_master(array &$variables) {
  // Fetch Language Entity Object.
  $language_master = $variables['elements']['#language_master'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
