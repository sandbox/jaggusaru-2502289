<?php

/**
 * @file
 * Contains education_level.page.inc.
 *
 * Page callback for Education level entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Education level templates.
 *
 * Default template: education_level.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_education_level(array &$variables) {
  // Fetch EducationLevel Entity Object.
  $education_level = $variables['elements']['#education_level'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
