<?php

namespace Drupal\education_level\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Education level entities.
 */
class EducationLevelViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['education_level']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Education level'),
      'help' => $this->t('The Education level ID.'),
    );

    return $data;
  }

}
