<?php

namespace Drupal\education_level\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Education level entities.
 *
 * @ingroup education_level
 */
class EducationLevelDeleteForm extends ContentEntityDeleteForm {

}
