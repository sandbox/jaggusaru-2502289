<?php

namespace Drupal\education_level;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Education level entity.
 *
 * @see \Drupal\education_level\Entity\EducationLevel.
 */
class EducationLevelAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\education_level\EducationLevelInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished education level entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published education level entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit education level entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete education level entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add education level entities');
  }

}
