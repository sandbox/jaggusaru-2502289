<?php
namespace Drupal\education_level;

use Drupal\education_level\Entity\EducationLevel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EducationLevelManager {

    public static function getEducationLevels() {
        $query = \Drupal::entityQuery('education_level');


        $nids = $query->execute();
        return EducationLevel::loadMultiple($nids);
    }

}
