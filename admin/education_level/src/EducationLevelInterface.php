<?php

namespace Drupal\education_level;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Education level entities.
 *
 * @ingroup education_level
 */
interface EducationLevelInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Education level name.
   *
   * @return string
   *   Name of the Education level.
   */
  public function getName();

  /**
   * Sets the Education level name.
   *
   * @param string $name
   *   The Education level name.
   *
   * @return \Drupal\education_level\EducationLevelInterface
   *   The called Education level entity.
   */
  public function setName($name);

  /**
   * Gets the Education level creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Education level.
   */
  public function getCreatedTime();

  /**
   * Sets the Education level creation timestamp.
   *
   * @param int $timestamp
   *   The Education level creation timestamp.
   *
   * @return \Drupal\education_level\EducationLevelInterface
   *   The called Education level entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Education level published status indicator.
   *
   * Unpublished Education level are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Education level is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Education level.
   *
   * @param bool $published
   *   TRUE to set this Education level to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\education_level\EducationLevelInterface
   *   The called Education level entity.
   */
  public function setPublished($published);

}
