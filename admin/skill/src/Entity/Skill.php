<?php

namespace Drupal\skill\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\skill\SkillInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Skill entity.
 *
 * @ingroup skill
 *
 * @ContentEntityType(
 *   id = "skill_master",
 *   label = @Translation("Skill"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\skill\SkillListBuilder",
 *     "views_data" = "Drupal\skill\Entity\SkillViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\skill\Form\SkillForm",
 *       "add" = "Drupal\skill\Form\SkillForm",
 *       "edit" = "Drupal\skill\Form\SkillForm",
 *       "delete" = "Drupal\skill\Form\SkillDeleteForm",
 *     },
 *     "access" = "Drupal\skill\SkillAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\skill\SkillHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "skill_master",
 *   admin_permission = "administer skill entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/skill_master/{skill_master}",
 *     "add-form" = "/admin/structure/skill_master/add",
 *     "edit-form" = "/admin/structure/skill_master/{skill_master}/edit",
 *     "delete-form" = "/admin/structure/skill_master/{skill_master}/delete",
 *     "collection" = "/admin/structure/skill_master",
 *   },
 *   field_ui_base_route = "skill_master.settings"
 * )
 */
class Skill extends ContentEntityBase implements SkillInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
        parent::preCreate($storage_controller, $values);
        $values += array(
            'user_id' => \Drupal::currentUser()->id(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->get('name')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name) {
        $this->set('name', $name);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
        return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($timestamp) {
        $this->set('created', $timestamp);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwner() {
        return $this->get('user_id')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnerId() {
        return $this->get('user_id')->target_id;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnerId($uid) {
        $this->set('user_id', $uid);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwner(UserInterface $account) {
        $this->set('user_id', $account->id());
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished() {
        return (bool) $this->getEntityKey('status');
    }

    /**
     * {@inheritdoc}
     */
    public function setPublished($published) {
        $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        $fields['id'] = BaseFieldDefinition::create('integer')
                ->setLabel(t('ID'))
                ->setDescription(t('The ID of the Skill entity.'))
                ->setReadOnly(TRUE);
        $fields['uuid'] = BaseFieldDefinition::create('uuid')
                ->setLabel(t('UUID'))
                ->setDescription(t('The UUID of the Skill entity.'))
                ->setReadOnly(TRUE);

        $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
                ->setLabel(t('Authored by'))
                ->setDescription(t('The user ID of author of the Skill entity.'))
                ->setRevisionable(TRUE)
                ->setSetting('target_type', 'user')
                ->setSetting('handler', 'default')
                ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
                ->setTranslatable(TRUE)
                ->setDisplayOptions('view', array(
                    'label' => 'hidden',
                    'type' => 'author',
                    'weight' => 0,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'entity_reference_autocomplete',
                    'weight' => 5,
                    'settings' => array(
                        'match_operator' => 'CONTAINS',
                        'size' => '60',
                        'autocomplete_type' => 'tags',
                        'placeholder' => '',
                    ),
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);
        
        $fields['name'] = BaseFieldDefinition::create('string')
                ->setLabel(t('Name'))
                ->setDescription(t('The name of the Skill entity.'))
                ->setSettings(array(
                    'max_length' => 50,
                    'text_processing' => 0,
                ))
                ->setDefaultValue('')
                ->setDisplayOptions('view', array(
                    'label' => 'above',
                    'type' => 'string',
                    'weight' => -4,
                ))
                ->setDisplayOptions('form', array(
                    'type' => 'string_textfield',
                    'weight' => -4,
                ))
                ->setDisplayConfigurable('form', TRUE)
                ->setDisplayConfigurable('view', TRUE);

        $fields['status'] = BaseFieldDefinition::create('boolean')
                ->setLabel(t('Publishing status'))
                ->setDescription(t('A boolean indicating whether the Skill is published.'))
                ->setDefaultValue(TRUE);

        $fields['langcode'] = BaseFieldDefinition::create('language')
                ->setLabel(t('Language code'))
                ->setDescription(t('The language code for the Skill entity.'))
                ->setDisplayOptions('form', array(
                    'type' => 'language_select',
                    'weight' => 10,
                ))
                ->setDisplayConfigurable('form', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
                ->setLabel(t('Created'))
                ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
                ->setLabel(t('Changed'))
                ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }

}
