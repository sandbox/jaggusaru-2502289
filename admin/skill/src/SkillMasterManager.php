<?php
namespace Drupal\skill;
use Drupal\skill\Entity\Skill;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SkillMasterManager{
    public static function getSkillMasters(){
        $query = \Drupal::entityQuery('skill_master');
           
            
        $nids = $query->execute();
        return Skill::loadMultiple($nids);
    }
}