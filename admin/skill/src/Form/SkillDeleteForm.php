<?php

namespace Drupal\skill\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Skill entities.
 *
 * @ingroup skill
 */
class SkillDeleteForm extends ContentEntityDeleteForm {

}
