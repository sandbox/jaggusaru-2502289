<?php

namespace Drupal\skill;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Skill entity.
 *
 * @see \Drupal\skill\Entity\Skill.
 */
class SkillAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\skill\SkillInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished skill entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published skill entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit skill entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete skill entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add skill entities');
  }

}
