<?php

/**
 * @file
 * Contains skill_master.page.inc.
 *
 * Page callback for Skill entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Skill templates.
 *
 * Default template: skill_master.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_skill_master(array &$variables) {
  // Fetch Skill Entity Object.
  $skill_master = $variables['elements']['#skill_master'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
