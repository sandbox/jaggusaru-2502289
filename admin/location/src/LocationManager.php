<?php
namespace Drupal\location;

use Drupal\location\Entity\Location;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LocationManager {

    public static function getLocations() {
        $query = \Drupal::entityQuery('location');


        $nids = $query->execute();
        return Location::loadMultiple($nids);
    }

}
