<?php

/**
 * @file
 * Contains relationship.page.inc.
 *
 * Page callback for Relationship entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Relationship templates.
 *
 * Default template: relationship.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_relationship(array &$variables) {
  // Fetch Relationship Entity Object.
  $relationship = $variables['elements']['#relationship'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
