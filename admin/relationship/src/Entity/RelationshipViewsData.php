<?php

namespace Drupal\relationship\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Relationship entities.
 */
class RelationshipViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['relationship']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Relationship'),
      'help' => $this->t('The Relationship ID.'),
    );

    return $data;
  }

}
