<?php

namespace Drupal\relationship\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Relationship entities.
 *
 * @ingroup relationship
 */
class RelationshipDeleteForm extends ContentEntityDeleteForm {

}
