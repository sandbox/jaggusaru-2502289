<?php

namespace Drupal\relationship;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Relationship entities.
 *
 * @ingroup relationship
 */
interface RelationshipInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Relationship name.
   *
   * @return string
   *   Name of the Relationship.
   */
  public function getName();

  /**
   * Sets the Relationship name.
   *
   * @param string $name
   *   The Relationship name.
   *
   * @return \Drupal\relationship\RelationshipInterface
   *   The called Relationship entity.
   */
  public function setName($name);

  /**
   * Gets the Relationship creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Relationship.
   */
  public function getCreatedTime();

  /**
   * Sets the Relationship creation timestamp.
   *
   * @param int $timestamp
   *   The Relationship creation timestamp.
   *
   * @return \Drupal\relationship\RelationshipInterface
   *   The called Relationship entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Relationship published status indicator.
   *
   * Unpublished Relationship are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Relationship is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Relationship.
   *
   * @param bool $published
   *   TRUE to set this Relationship to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\relationship\RelationshipInterface
   *   The called Relationship entity.
   */
  public function setPublished($published);

}
