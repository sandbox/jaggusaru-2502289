<?php
namespace Drupal\relationship;
use Drupal\relationship\Entity\Relationship;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RelationshipManager{
    public static function getRelationships(){
        $query = \Drupal::entityQuery('relationship');
           
            
        $nids = $query->execute();
        return Relationship::loadMultiple($nids);
    }
}