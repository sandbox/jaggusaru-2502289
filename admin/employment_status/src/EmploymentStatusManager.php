<?php

namespace Drupal\employment_status;

use Drupal\employment_status\Entity\EmploymentStatus;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EmploymentStatusManager {

    public static function getEmploymentStatus() {
        $query = \Drupal::entityQuery('employment_status');


        $nids = $query->execute();
        return EmploymentStatus::loadMultiple($nids);
    }

}
