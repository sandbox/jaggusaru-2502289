<?php

namespace Drupal\employment_status\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Employment status entities.
 */
class EmploymentStatusViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['employment_status']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Employment status'),
      'help' => $this->t('The Employment status ID.'),
    );

    return $data;
  }

}
