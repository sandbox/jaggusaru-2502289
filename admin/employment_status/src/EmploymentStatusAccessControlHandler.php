<?php

namespace Drupal\employment_status;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Employment status entity.
 *
 * @see \Drupal\employment_status\Entity\EmploymentStatus.
 */
class EmploymentStatusAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\employment_status\EmploymentStatusInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished employment status entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published employment status entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit employment status entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete employment status entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add employment status entities');
  }

}
