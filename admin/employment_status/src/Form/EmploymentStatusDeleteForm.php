<?php

namespace Drupal\employment_status\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Employment status entities.
 *
 * @ingroup employment_status
 */
class EmploymentStatusDeleteForm extends ContentEntityDeleteForm {

}
