<?php

namespace Drupal\employment_status;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Employment status entities.
 *
 * @ingroup employment_status
 */
interface EmploymentStatusInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Employment status name.
   *
   * @return string
   *   Name of the Employment status.
   */
  public function getName();

  /**
   * Sets the Employment status name.
   *
   * @param string $name
   *   The Employment status name.
   *
   * @return \Drupal\employment_status\EmploymentStatusInterface
   *   The called Employment status entity.
   */
  public function setName($name);

  /**
   * Gets the Employment status creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Employment status.
   */
  public function getCreatedTime();

  /**
   * Sets the Employment status creation timestamp.
   *
   * @param int $timestamp
   *   The Employment status creation timestamp.
   *
   * @return \Drupal\employment_status\EmploymentStatusInterface
   *   The called Employment status entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Employment status published status indicator.
   *
   * Unpublished Employment status are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Employment status is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Employment status.
   *
   * @param bool $published
   *   TRUE to set this Employment status to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\employment_status\EmploymentStatusInterface
   *   The called Employment status entity.
   */
  public function setPublished($published);

}
