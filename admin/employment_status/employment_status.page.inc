<?php

/**
 * @file
 * Contains employment_status.page.inc.
 *
 * Page callback for Employment status entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Employment status templates.
 *
 * Default template: employment_status.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_employment_status(array &$variables) {
  // Fetch EmploymentStatus Entity Object.
  $employment_status = $variables['elements']['#employment_status'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
