<?php 
namespace Drupal\leave\Form; 
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface; 
class LeaveForm extends FormBase{

	public function getFormId(){
		return 'leave_add_form';
		}
	public function buildForm(array $form, FormStateInterface $form_state){
		$form['company_name']['#type'] = 'textfield';
		$form['company_name']['#title'] = 'textfield';
		$form['company_name']['#prefix'] = '<div class="row"><div class="panel panel-primary"><div class="panel-heading"><h3 class="panel-titile"></h3></div><div class="panel-body"><div class="col-sm-3"><div class="form-group">';
		$form['company_name']['#attributes']['class'][] = 'form-control';
		$form['company_name']['#suffix'] = '</div>div>';
			$form['company_name'] = array(
				'#type' => 'textfield',
				'#title' => $this->t('Company name'),
				'#prefix' => '',
			);
			$form['company_name1'] = array(
				'#type' => 'textfield',
				'#title' => $this->t('Company name'),
			);
			$form['company_name2'] = array(
				'#type' => 'textfield',
				'#title' => $this->t('Company name'),
			);
			$form['company_name3'] = array(
				'#type' => 'textfield',
				'#title' => $this->t('Company name'),
			);
			$form['company_name4'] = array(
				'#type' => 'textfield',
				'#title' => $this->t('Company name'),
				'#suffix' => '</div></div>',
			);
			return $form;
	}
	public function validateForm(array &$form, FormStateInterface $form_state) {
	}
	public function submitForm(array &$form, FormStateInterface $form_state) {
	}
}
