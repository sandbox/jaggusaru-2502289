<?php

namespace Drupal\leave\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 *
 * @package Drupal\leave\Controller
 */
class DefaultController extends ControllerBase {
  /**
   * Test.
   *
   * @return string
   *   Return Hello string.
   */
  public function test($name) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: test with parameter(s): $name'),
    ];
  }

}
