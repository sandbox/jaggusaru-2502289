<?php

namespace Drupal\leave\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the leave module.
 */
class DefaultControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "leave DefaultController's controller functionality",
      'description' => 'Test Unit for module leave and controller DefaultController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests leave functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module leave.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
